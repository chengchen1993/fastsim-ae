from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import keras
from keras.layers import Activation, Dense, Input
from keras.layers import Conv2D, Flatten
from keras.layers import Reshape, Conv2DTranspose
from keras.layers.advanced_activations import LeakyReLU
from keras.layers import Lambda
from keras.models import Model
from keras.models import load_model
from keras.models import save_model
from keras import backend as K
from keras.optimizers import Adam
from keras.backend import random_normal
from keras.callbacks import ReduceLROnPlateau
from keras.datasets import mnist
from keras.callbacks import ModelCheckpoint
from keras.callbacks import EarlyStopping
from keras.callbacks import Callback
from keras.callbacks import LearningRateScheduler
from keras.utils import plot_model
import numpy as np
from sklearn.model_selection import train_test_split 
import matplotlib.pyplot as plt
import sys
import subprocess
from subprocess import Popen 
from optparse   import OptionParser
from time       import gmtime, strftime
from array import array 
from root_numpy import root2array, array2tree, rec2array, fill_hist
import h5py
import ROOT
import math
import random
import uproot
import os
from ROOT import gROOT, TLorentzVector,TGraphAsymmErrors, TPaveLabel, TPie, gStyle, gSystem, TGaxis, TStyle, TLatex, TString, TF1,TFile,TLine, TLegend, TH1D,TH2D,THStack, TGraph,TMultiGraph, TGraphErrors,TChain, TCanvas, TMatrixDSym, TMath, TText, TPad, TVectorD, RooFit, RooArgSet, RooArgList, RooArgSet, RooAbsData, RooAbsPdf, RooAddPdf, RooWorkspace, RooExtendPdf,RooCBShape, RooLandau, RooFFTConvPdf, RooGaussian, RooBifurGauss, RooArgusBG,RooDataSet, RooExponential,RooBreitWigner, RooVoigtian, RooNovosibirsk, RooRealVar,RooFormulaVar, RooDataHist, RooHist,RooCategory, RooChebychev, RooSimultaneous, RooGenericPdf,RooConstVar, RooKeysPdf, RooHistPdf, RooEffProd, RooProdPdf, TIter, kTRUE, kFALSE, kGray, kRed, kDashed, kGreen,kAzure, kOrange, kBlack,kBlue,kYellow,kCyan, kMagenta, kWhite

parser = OptionParser()
parser.add_option('--maxevents',action="store",type="int",dest="maxevents",default=2000000000000000)
parser.add_option('--epochs',action="store",type="int",dest="epochs",default=1)
parser.add_option('--train_apply',action="store",type="int",dest="train_apply",default=1)
(options, args) = parser.parse_args()

Pi=3.1415926535898 
M_Mu=0.10565
M_Pi=0.13957
M_K=0.49367
M_D0=1.86483
M_Dst=2.01026
M_B0=5.27963
Mean_pre=[]
Std_pre=[]
Mean=[]
Std=[]
gamma=0
beta=1.
epsilon=K.epsilon()
weight0=0.
weight_end=1.
epoch0=500
epoch_rise=1000
factor=(weight_end-weight0)/(epoch_rise-epoch0)
Range=1.
Range2=5.
weight_mqe=1.
weight_m=weight_mqe
weight_q=weight_mqe
weight_e=weight_mqe
sqrt_weight_m=math.sqrt(weight_m)
sqrt_weight_q=math.sqrt(weight_q)
sqrt_weight_e=math.sqrt(weight_e)
initial_lrate = 0.001
lr_decay=1.
pt_cut=30

class MyCallback(Callback):
    def __init__(self, weight1, weight2, weight3):
         self.weight1 = weight1
         self.weight2 = weight2
         self.weight3 = weight3
    def on_epoch_end(self, epoch, logs={}):
         print("weight")
         print(K.get_value(self.weight1))
         print(K.get_value(self.weight2))
         print(K.get_value(self.weight3))

def scheduler(epoch):
    lr = initial_lrate
    lr = initial_lrate/(1.+lr_decay*epoch)
    return lr

def comput(inputs):
    # Inputs are true values of prediction for REC
    x = inputs
    x1,x2,x3,x4,x5,x6,x7,x8,x9=K.tf.split(x,9,axis=1,num=None)

    Met=K.sqrt(x8*x8+x9*x9)   
    Met_phi=K.tf.math.atan(x9/(K.abs(x8)+epsilon))
    sign_y=(x9+epsilon)/(K.abs(x9)+epsilon)  # 1 or -1
    sign_x=(x8+epsilon)/(K.abs(x8)+epsilon)
    Met_phi=Met_phi*sign_x+Pi*sign_y*(sign_x-1.)/(-2.)

    Pt_Mu=K.sqrt(x1*x1+x2*x2)
    Pz_Mu=x3   
    P_Mu=K.sqrt(Pt_Mu*Pt_Mu+Pz_Mu*Pz_Mu)
    E_Mu=K.sqrt(K.relu(Pt_Mu*Pt_Mu+Pz_Mu*Pz_Mu+M_Mu*M_Mu))
    Phi_Mu=K.tf.math.atan(x2/(K.abs(x1)+epsilon))
    sign_y=(x2+epsilon)/(K.abs(x2)+epsilon)  # 1 or -1
    sign_x=(x1+epsilon)/(K.abs(x1)+epsilon)
    Phi_Mu=Phi_Mu*sign_x+Pi*sign_y*(sign_x-1.)/(-2.)

    costheta=Pz_Mu/(K.relu(P_Mu)+epsilon)
    sintheta=Pt_Mu/(K.relu(P_Mu)+epsilon)
    tan_half_theta=(1-costheta)/(K.relu(sintheta)+epsilon)
    Eta_Mu=-K.tf.math.log(K.relu(tan_half_theta)+epsilon)


    Pt_J1=K.sqrt(x5*x5+x4*x4)
    Pz_J1=x6
    P_J1=K.sqrt(Pt_J1*Pt_J1+Pz_J1*Pz_J1)
    M_J1=K.exp(x7)
    E_J1=K.sqrt(K.relu(Pt_J1*Pt_J1+Pz_J1*Pz_J1+M_J1*M_J1))
    Phi_J1=K.tf.math.atan(x5/(K.abs(x4)+epsilon))
    sign_y=(x5+epsilon)/(K.abs(x5)+epsilon)  # 1 or -1
    sign_x=(x4+epsilon)/(K.abs(x4)+epsilon)
    Phi_J1=Phi_J1*sign_x+Pi*sign_y*(sign_x-1.)/(-2.)

    costheta=Pz_J1/(K.relu(P_J1)+epsilon)
    sintheta=Pt_J1/(K.relu(P_J1)+epsilon)
    tan_half_theta=(1-costheta)/(K.relu(sintheta)+epsilon)
    Eta_J1=-K.tf.math.log(K.relu(tan_half_theta)+epsilon)

    ST=Met+Pt_Mu+Pt_J1

    MT=K.sqrt(K.relu((E_Mu+E_J1)*(E_Mu+E_J1)-(Pz_Mu+Pz_J1)*(Pz_Mu+Pz_J1)))

    return K.concatenate([ST, MT, Pt_Mu, Eta_Mu, Phi_Mu, Pt_J1, Eta_J1, Phi_J1, E_J1, M_J1, Met, Met_phi], axis=1) 

def Comput_BN(inputs):
    x_mae, x_quantile, mc = inputs

    x_mean = x_mae
 
    x_std = K.abs(x_mae-x_quantile)

    x=K.random_normal(shape=K.shape(x_mean), mean=x_mean, stddev=x_std) 
    
    x1,x2,x3,x4,x5,x6,x7,x8,x9=K.tf.split(x,9,axis=1,num=None)
    
    mc1,mc2,mc3,mc4,mc5,mc6,mc7,mc8,mc9=K.tf.split(mc,9,axis=1,num=None)

    # Return to true values: pre
    x1=x1*(Std[0]+epsilon)/beta+Mean[0]
    x2=x2*(Std[1]+epsilon)/beta+Mean[1]
    x3=x3*(Std[2]+epsilon)/beta+Mean[2]
    x4=x4*(Std[3]+epsilon)/beta+Mean[3]
    x5=x5*(Std[4]+epsilon)/beta+Mean[4]
    x6=x6*(Std[5]+epsilon)/beta+Mean[5]
    x7=x7*(Std[6]+epsilon)/beta+Mean[6]
    x8=x8*(Std[7]+epsilon)/beta+Mean[7]
    x9=x9*(Std[8]+epsilon)/beta+Mean[8]


    # Fix values after BN: pre=rec
    x1_fix=(x1-Mean[0])*beta/(Std[0]+epsilon)
    x2_fix=(x2-Mean[1])*beta/(Std[1]+epsilon)
    x3_fix=(x3-Mean[2])*beta/(Std[2]+epsilon)
    x4_fix=(x4-Mean[3])*beta/(Std[3]+epsilon)
    x5_fix=(x5-Mean[4])*beta/(Std[4]+epsilon)
    x6_fix=(x6-Mean[5])*beta/(Std[5]+epsilon)
    x7_fix=(x7-Mean[6])*beta/(Std[6]+epsilon)
    x8_fix=(x8-Mean[7])*beta/(Std[7]+epsilon)
    x9_fix=(x9-Mean[8])*beta/(Std[8]+epsilon)

    x_fix=K.concatenate([x1_fix, x2_fix, x3_fix, x4_fix, x5_fix, x6_fix, x7_fix, x8_fix, x9_fix], axis=1)
   
    inputs=K.concatenate([x1, x2, x3, x4, x5, x6, x7, x8, x9], axis=1)

    comput_true = comput(inputs)

    return [K.concatenate([x_fix, comput_true], axis=1), x_mae, x_quantile]

def train_and_apply():
    np.random.seed(1)
    ROOT.gROOT.SetBatch()

    st=0
    en=50
    for i in range(st,en):
       if i<10000:   input_file='./h5_new_2/WtoMuNu_Jets_13TeV_PU200_TEST_'+str(i)+'.h5'
       if i>=10000:  input_file='./h5_new/WtoMuNu_Jets_13TeV_PU200_'+str(i-10000)+'.h5'
       try:
            f = h5py.File(input_file,'r')
            f.keys()
    
            if i==st:
              mc=f['gen_basicFeatures'][:]
              rec=f['reco_basicFeatures'][:]
            else:
              mc = np.vstack((mc,f['gen_basicFeatures'][:]))
              rec = np.vstack((rec,f['reco_basicFeatures'][:]))
       except:
            1>0
    # name [b'muPx' b'muPy' b'muPz' b'j1Px' b'j1Py' b'j1Pz' b'J1Mass' b'MET' b'phiMET']    

    nvariable=9

    # Remove jet mass<=0
    n_delete = []
    for i in range(0,mc.shape[0]):
                  if mc[i][6]<epsilon or rec[i][6]<epsilon:  n_delete.append(i)

    mc = np.delete(mc, n_delete, axis=0)
    rec = np.delete(rec, n_delete, axis=0)

    for i in range(0,mc.shape[0]):
                MET_mc=mc[i][7]
                PHIMET_mc=mc[i][8]
                MET_rec=rec[i][7]
                PHIMET_rec=rec[i][8]
                mc[i][6]=math.log(mc[i][6])
                mc[i][7]=MET_mc*math.cos(PHIMET_mc)
                mc[i][8]=MET_mc*math.sin(PHIMET_mc)
                rec[i][6]=math.log(rec[i][6])
                rec[i][7]=MET_rec*math.cos(PHIMET_rec)
                rec[i][8]=MET_rec*math.sin(PHIMET_rec)
                

    mc_0=mc
    rec_0=rec
    rec_minus=rec-mc

    # BN normalization   
    # 9 true values
    global Mean
    global Std
    # 9 rec-mc values
    global Mean_pre
    global Std_pre

    mc_and_rec = np.vstack((mc, rec))
    ar=np.array(mc_and_rec)
    ar = K.constant(ar)
    a, m, s=BN(ar)
    sess = K.tf.Session()
    mc_and_rec=np.array(sess.run(a))
    for i in range(0,nvariable):
           Mean.append(sess.run(m)[0,i])
           Std.append(sess.run(s)[0,i])

    mc = mc_and_rec[:mc.shape[0],:]
    rec = mc_and_rec[mc.shape[0]:,:]

    # Comput Lambda layer variables for GEN and RECO 
   
    MC=K.constant(mc_0)
    MC=comput(MC)
    MC=sess.run(MC)

    REC=K.constant(rec_0)
    REC=comput(REC)
    REC=sess.run(REC)

    MC=np.column_stack((mc,MC))    
    rec=np.column_stack((rec,REC))

    ar=np.array(rec_minus)
    ar = K.constant(ar)
    a, m, s=BN(ar)
    rec_minus=np.array(sess.run(a))
    for i in range(0,nvariable):
           Mean_pre.append(sess.run(m)[0,i])
           Std_pre.append(sess.run(s)[0,i])

    # Transformation
    rec_t = mc+(rec[:,0:nvariable]-mc)*(Std_pre[6]/Std[6])*Std[0:nvariable]/Std_pre 

    # Define train and test (MC, MC_test are only used for plotting)
    test_size=0.3
    random_state=0 
    MC_train, MC_test, x_train, x_test, y_train, y_test, y_train_t, y_test_t = train_test_split(MC, mc, rec, rec_t, test_size=test_size, random_state=random_state)

    N=mc.shape[0]
    N_test=x_test.shape[0]  

    # Network parameters
    input_shape = (x_train.shape[1],)
    batch_size = 128
    latent_dim = 100

    # Build the Autoencoder Model
    # First build the Encoder Model
    inputs = Input(shape=input_shape, name='Gen_input')

    # Shape info needed to build Decoder Model
    shape = K.int_shape(inputs)

    print("-------------")
    print(shape)

    x1 = inputs

    x1 = Dense(latent_dim, name='Encoder_mae')(x1)
    x1 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_1_mae')(x1)

    # Generate the latent vector
    latent1 = Dense(latent_dim, name='Latent_vector_1_mae')(x1)
    latent1 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_2_mae')(latent1)

    x1 = Dense(latent_dim, name='Latent_vector_2_mae')(latent1)
    x1 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_3_mae')(x1)

    x1 = Dense(latent_dim, name='Latent_vector_3_mae')(x1)
    x1 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_4_mae')(x1)

    x1 = Dense(latent_dim, name='Latent_vector_4_mae')(x1)
    x1 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_5_mae')(x1)

    x1 = Dense(shape[1], name='Decoder_mae')(x1)
    x1 = Activation('linear', name='Linear_mae')(x1)

    x2 = inputs

    x2 = Dense(latent_dim, name='Encoder_quantile')(x2)
    x2 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_1_quantile')(x2)

    # Generate the latent vector
    latent2 = Dense(latent_dim, name='Latent_vector_1_quantile')(x2)
    latent2 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_2_quantile')(latent2)

    x2 = Dense(latent_dim, name='Latent_vector_2_quantile')(latent2)
    x2 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_3_quantile')(x2)

    x2 = Dense(latent_dim, name='Latent_vector_3_quantile')(x2)
    x2 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_4_quantile')(x2)

    x2 = Dense(latent_dim, name='Latent_vector_4_quantile')(x2)
    x2 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_5_quantile')(x2)

    x2 = Dense(shape[1], name='Decoder_quantile')(x2)
    x2 = Activation('linear', name='Linear_quantile')(x2)
  
    output1, output2, output3 = Lambda(Comput_BN, name='Comput_BN')([x1, x2, inputs])


    autoencoder = Model(inputs=[inputs], outputs=[output1, output2, output3], name='autoencoder')
    autoencoder.summary()

    if options.train_apply==0:

        os.system("rm weights-*.h5")

        # Train the autoencoder 
   
        weight1=K.variable(0.)
        weight2=K.variable(0.5)
        weight3=K.variable(0.5)
  
        optimizer = Adam(lr=0.001)
        lr_metric = get_lr_metric(optimizer)
 
        autoencoder.compile(loss=['mae', 'mae', quantile_loss], loss_weights=[weight1, weight2, weight3], optimizer=optimizer, metrics=[lr_metric], normalize=1)
   
        lr = LearningRateScheduler(scheduler)

        # val_loss: total weighted loss; val_Comput_BN_loss: loss of last output before being weighted

        filepath="weights-{epoch:02d}-{val_loss:.7f}.h5"       

        checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=False, save_weights_only=True, mode='auto')
   
        autoencoder.fit(x_train, [np.zeros( (x_train.shape[0], 21) ), y_train_t, y_train_t],
                    validation_data=(x_test, [np.zeros( (x_test.shape[0], 21) ), y_test_t, y_test_t]),
                    epochs=options.epochs,
                    batch_size=batch_size,
                    callbacks=[MyCallback(weight1, weight2, weight3), checkpoint, lr])

        val_loss_min=100.
        final=0
        os.system("ls weights-*.h5 > file.log")
        file=open("file.log")
        for line in file:
                      line_split=line.split("-")
                      val_loss=float((line_split[2])[0:7])
                      n=int(line_split[1])
                      if val_loss<=val_loss_min:
                                  final=line
                                  val_loss_min=val_loss

        print("-------The best weight is: ---------")
        print(final)

        plot_model(autoencoder, to_file='./Plots/model.png', show_shapes=True)

    if options.train_apply!=0:

        branch_rec=["p_{x}^{\mu}","p_{y}^{\mu}","p_{z}^{\mu}","p_{x}^{J1}","p_{y}^{J1}","p_{z}^{J1}","Log(M^{J1})","MET_{x}","MET_{y}","S_{T}","M_{T}","p_{T}^{\mu}","\eta^{\mu}","\phi^{\mu}","p_{T}^{J1}","\eta^{J1}","\phi^{J1}","E^{J1}","M^{J1}","MET","MET^{\phi}","p_{x}^{\mu}(residual)","p_{y}^{\mu}(residual)","p_{z}^{\mu}(residual)","p_{x}^{J1}(residual)","p_{y}^{J1}(residual)","p_{z}^{J1}(residual)","Log(M^{J1})(residual)","MET_{x}(residual)","MET_{y}(residual)","S_{T}(residual)","M_{T}(residual)","p_{T}^{\mu}(residual)","\eta^{\mu}(residual)","\phi^{\mu}(residual)","p_{T}^{J1}(residual)","\eta^{J1}(residual)","\phi^{J1}(residual)","E^{J1}(residual)","M^{J1}(residual)","MET(residual)","MET^{\phi}(residual)"]

        x_test_tmp=x_test
 
        mc=mc.tolist()
        rec=rec.tolist()
        x_test=x_test.tolist()
        y_test=y_test.tolist()

        print(x_test[0][0]) 
        for i in range(0,nvariable):
                 for k in range(0,N_test):
                      MC_test[k][i]=MC_test[k][i]*(Std[i]+epsilon)/beta+Mean[i]
                      y_test[k][i]=y_test[k][i]*(Std[i]+epsilon)/beta+Mean[i]
                      x_test[k][i]=x_test[k][i]*(Std[i]+epsilon)/beta+Mean[i]

        # Draw Model Plots
        os.system("rm -rf Plots")
        os.system("mkdir Plots")

        print(np.array(x_test_tmp)-np.array(x_test))

        N_model=1
 
        for n in range(0,N_model):

            exec("autoencoder_"+str(n)+" = Model(inputs=[inputs], outputs=[output1, output2, output3], name='autoencoder')")
            eval("autoencoder_"+str(n)).summary()

            if n==0: weights_file='./weights-100-0.3044473.h5'
            if n==1: weights_file='../wjets_2/weights-100-0.3045673.h5'
            if n==2: weights_file='../wjets_3/weights-100-0.3049214.h5'
            if n==3: weights_file='../wjets_4/weights-100-0.3055171.h5'
            if n==4: weights_file='../wjets_5/weights-100-0.3046580.h5'
            if n==5: weights_file='../wjets_6/weights-100-0.3053142.h5'
            if n==6: weights_file='../wjets_7/weights-100-0.3050237.h5'
            if n==7: weights_file='../wjets_8/weights-100-0.3051775.h5'
            if n==8: weights_file='../wjets_9/weights-100-0.3052909.h5'
            if n==9: weights_file='../wjets_10/weights-100-0.3043786.h5'
            if n==10: weights_file='../wjets_11/weights-100-0.3047219.h5'
    
            eval("autoencoder_"+str(n)).load_weights(weights_file, by_name=True)

            # Predict the Autoencoder output from corrupted test imformation
            exec("x_decoded_pre_"+str(n)+" = autoencoder_"+str(n)+".predict(x_test_tmp)")
            exec("x_decoded_norm_"+str(n)+"=x_decoded_pre_"+str(n)+"[0]")

            for i in range(0,nvariable):
           #      for k in range(0,N_test):
           #           exec("x_decoded_norm_"+str(n)+"[k][i]=x_decoded_norm_"+str(n)+"[k][i]*(Std[i]+epsilon)/beta+Mean[i]")
                      exec("x_decoded_norm_"+str(n)+"[:,i]=x_decoded_norm_"+str(n)+"[:,i]*(Std[i]+epsilon)/beta+Mean[i]")
    
            # Draw Comparision Plots
            Epochs=(weights_file.split("-"))[1]
            log_file="draw_loss.log"
            
            # Final-selection: abs(Mu_eta)<2.4, Mu_pt>20, abs(J1_eta)<2.4, J1_pt>30
   #         M_delete = []
   #         for i in range(0,N):
   #                if MC[i][2+9]<-20 or abs(MC[i][3+9])>112.4 or MC[i][5+9]<-30 or abs(MC[i][6+9])>112.4 or rec[i][2+9]<20 or abs(rec[i][3+9])>2.4 or rec[i][5+9]<30 or abs(rec[i][6+9])>2.4:  M_delete.append(i)
    
   #         exec("MC_"+str(n) +" = np.delete(MC, M_delete, axis=0)")
   #         exec("rec_"+str(n) +" = np.delete(rec, M_delete, axis=0)")
   
            M_test_delete = []
            for i in range(0,N_test):
                      Deta=y_test[i][12]-MC_test[i][12]
                      Dphi=Pi-abs(abs(y_test[i][13]-MC_test[i][13])-Pi)
                      Dr=math.sqrt(Deta*Deta+Dphi*Dphi)
                      dr1=DR(y_test[i][12],y_test[i][13],y_test[i][15],y_test[i][16])  # DR(Rec Muon, Rec Jet)

                      if dr1<0.5 or Dr>1.5 or MC_test[i][2+9]<-20 or abs(MC_test[i][3+9])>112.4 or MC_test[i][5+9]<-30 or abs(MC_test[i][6+9])>112.4 or y_test[i][2+9]<20 or abs(y_test[i][3+9])>2.4 or y_test[i][5+9]<30 or abs(y_test[i][6+9])>2.4 or eval("x_decoded_norm_"+str(n))[i][2+9]<20 or abs(eval("x_decoded_norm_"+str(n))[i][3+9])>2.4 or eval("x_decoded_norm_"+str(n))[i][5+9]<30 or abs(eval("x_decoded_norm_"+str(n))[i][6+9])>2.4:  M_test_delete.append(i)
    
            exec("MC_test_"+str(n) +" = np.delete(MC_test, M_test_delete, axis=0)")
            exec("x_test_"+str(n)+ " = np.delete(x_test, M_test_delete, axis=0)")
            exec("y_test_"+str(n)+ " = np.delete(y_test, M_test_delete, axis=0)")
            exec("x_decoded_"+str(n)+ "= np.delete(x_decoded_norm_"+str(n)+", M_test_delete, axis=0)")
            print(("------test1"))
       #     print(MC_test)
                 
          #  MC=np.array(MC)
          #  rec=np.array(rec)
          #  MC_test=np.array(MC_test)
          #  x_test=np.array(x_test)
          #  y_test=np.array(y_test)
          #  x_decoded=np.array(eval("x_decoded_"+str(n)))

        MC_test=eval("MC_test_"+str(0))
        x_test=eval("x_test_"+str(0))
        y_test=eval("y_test_"+str(0))
        x_decoded=eval("x_decoded_"+str(0))

    #    print("-------------------")
    #    print(eval("x_decoded_"+str(0)).shape[0])
    #    print(eval("x_decoded_"+str(1)).shape[0])
    #    print(eval("x_decoded_"+str(2)).shape[0])

     #   print(eval("x_test_"+str(0)).shape[0])
     #   print(eval("x_test_"+str(1)).shape[0])
      #  print(eval("x_test_"+str(2)).shape[0])

        MC_test=np.array(MC_test)
        x_test=np.array(x_test)
        y_test=np.array(y_test)
        x_decoded=np.array(x_decoded)

        # Draw 21 REC-GEN variables:
        for j in range(0, nvariable+12):
            nbin=50
            arr=(x_decoded-MC_test)/(abs(MC_test)+epsilon)
            [m,s]=mean_std_cut(arr[:,j],0.05)
            sess = K.tf.Session()
            M=sess.run(m)
            S=sess.run(s)

            min=M-10.*S
            max=M+10.*S

            lbin=str(round((max-min)/nbin,4))
            c = TCanvas("c","c", 700,700)
            fPads1 = TPad("pad1", "Run2", 0.0, 0.39, 1.00, 1.00)
            fPads2 = TPad("pad2", "", 0.00, 0.00, 1.00, 0.39)
            fPads1.SetTopMargin(  0.15)
            fPads1.SetBottomMargin( 0.007)
            fPads1.SetLeftMargin(   0.10)
            fPads1.SetRightMargin(  0.03)
            fPads2.SetLeftMargin(   0.10 )
            fPads2.SetRightMargin(  0.03)
            fPads2.SetBottomMargin( 0.25)
            fPads1.Draw()
            fPads2.Draw()
            fPads1.cd()
            xtitle=branch_rec[j+21]
            ytitle="Events/"+lbin
            h_rec=TH1D("h_rec",""+";%s;%s"%(xtitle,ytitle),nbin,min,max)
            h_rec.Sumw2()
            h_pre=TH1D("h_pre",""+";%s;%s"%(xtitle,ytitle),nbin,min,max)
            h_pre.Sumw2()

            for v in range(1,N_model):
                      exec("h_pre_"+str(v)+"=TH1D('','',nbin,min,max)")
                      eval("h_pre_"+str(v)).Sumw2()
                      for i in range(eval("x_decoded_"+str(v)).shape[0]):
                               if abs(eval("MC_test_"+str(v))[i][j])<epsilon:
                                     eval("h_pre_"+str(v)).Fill((eval("x_decoded_"+str(v))[i][j]-eval("MC_test_"+str(v))[i][j])/epsilon)
                               else:
                                     eval("h_pre_"+str(v)).Fill((eval("x_decoded_"+str(v))[i][j]-eval("MC_test_"+str(v))[i][j])/eval("MC_test_"+str(v))[i][j])
                   #   exec("h_pre"+str(v)+"=UnderOverFlow1D(h_pre_"+str(v)+")")

            for i in range(y_test.shape[0]):
                      if abs(MC_test[i][j])<epsilon:
                             h_rec.Fill((y_test[i][j]-MC_test[i][j])/epsilon)
                             h_pre.Fill((x_decoded[i][j]-MC_test[i][j])/epsilon)
                      else:
                             h_rec.Fill((y_test[i][j]-MC_test[i][j])/MC_test[i][j])
                             h_pre.Fill((x_decoded[i][j]-MC_test[i][j])/MC_test[i][j])
        #    h_rec=UnderOverFlow1D(h_rec)
        #    h_pre=UnderOverFlow1D(h_pre)


            for v in range(1,N_model):
                   eval("h_pre_"+str(v)).Scale(h_pre.Integral()/eval("h_pre_"+str(v)).Integral());
          #  print(h_pre.Integral());
           # print(eval("h_pre_"+str(1)).Integral());
           # print(eval("h_pre_"+str(2)).Integral());

            x = [];
            y = [];
            eyl = [];
            eyh = [];

            for v in range(1,nbin+1):
                       x.append((max-min)*(v-0.5)/nbin+min);
                       y.append(h_pre.GetBinContent(v));
                       ar=[];
                       ar.append(h_pre.GetBinContent(v));
                       for vv in range(1,N_model):
                                ar.append(eval("h_pre_"+str(vv)).GetBinContent(v));
                       ar = sorted(ar)
                       eyl.append(h_pre.GetBinContent(v)-ar[0])
                       eyh.append(ar[len(ar)-1]-h_pre.GetBinContent(v))

            for v in range(1,nbin+1):
                       rms=0
                       for vv in range(1,N_model):
                              rms=rms+pow((h_pre.GetBinContent(v)-eval("h_pre_"+str(vv)).GetBinContent(v)),2)
                       if N_model>1:
                              rms=math.sqrt(rms/(N_model-1))
                       else:
                              rms=0
                       h_pre.SetBinError(v,math.sqrt(rms*rms+h_pre.GetBinError(v)*h_pre.GetBinError(v)))

            #CHI2 test
            NBIN=30
            MIN=min
            MAX=max

            h_REC=TH1D("h_rec",""+";%s;%s"%(xtitle,ytitle),NBIN,MIN,MAX)
            h_REC.Sumw2()
            h_PRE=TH1D("h_pre",""+";%s;%s"%(xtitle,ytitle),NBIN,MIN,MAX)
            h_PRE.Sumw2()
            for i in range(y_test.shape[0]):
                      if abs(MC_test[i][j])<epsilon:
                             h_REC.Fill((y_test[i][j]-MC_test[i][j])/epsilon)
                             h_PRE.Fill((x_decoded[i][j]-MC_test[i][j])/epsilon)
                      else:
                             h_REC.Fill((y_test[i][j]-MC_test[i][j])/MC_test[i][j])
                             h_PRE.Fill((x_decoded[i][j]-MC_test[i][j])/MC_test[i][j])

            p_value=h_REC.Chi2Test(h_PRE,"UF,OF")
            if p_value>0.99:
                 p_value=1.-float("%.2g" % (1.-p_value))
            else:
                 p_value=round(p_value,3)

            if j==0 or j==1 or j==2 or j==12 or j==13 or j==15 or j==16:
                   maxR = 3
                   minR = -1
            else:
                   maxR = 2
                   minR = 0

            maxY = TMath.Max( h_rec.GetMaximum(), h_pre.GetMaximum() )
            h_rec.SetLineColor(2)
            h_rec.SetFillStyle(0)
            h_rec.SetLineWidth(2)
            h_rec.SetLineStyle(1)
            h_pre.SetLineColor(3)
            h_pre.SetFillStyle(0)
            h_pre.SetLineWidth(2)
            h_pre.SetLineStyle(1)
            h_rec.GetYaxis().SetRangeUser( 0 , maxY*1.1 )
            h_rec.GetYaxis().SetTitleSize(0.06)
            h_rec.GetYaxis().SetTitleOffset(0.82)
            h_rec.SetStats(0)
            h_pre.SetStats(0)
            h_rec.Draw("e HIST")
            h_pre.Draw("same e HIST")

            gr = TGraphAsymmErrors();
            gr.SetMarkerStyle(21);
            gr.SetMarkerColor(kOrange);
            gr.SetMarkerSize(0.0);
            gr.SetLineWidth(2);
            gr.SetLineColor(kOrange);

            for v in range(1,nbin+1):
                   gr.SetPoint(v,x[v-1],y[v-1])
                   gr.SetPointEYlow(v,eyl[v-1])
                   gr.SetPointEYhigh(v,eyh[v-1])
          #  gr.Draw("p");

            theLeg = TLegend(0.68, 0.45, 0.99, 0.82, "", "NDC")
            theLeg.SetName("theLegend")
            theLeg.SetBorderSize(0)
            theLeg.SetLineColor(0)
            theLeg.SetFillColor(0)
            theLeg.SetFillStyle(0)
            theLeg.SetLineWidth(0)
            theLeg.SetLineStyle(0)
            theLeg.SetTextFont(42)
            theLeg.SetTextSize(.05)
            theLeg.AddEntry(h_rec,"(x_{R}-x_{G})/x_{G}","L")
            theLeg.AddEntry(h_pre,"(x_{DL}-x_{G})/x_{G}","L")
            theLeg.SetY1NDC(0.9-0.05*6-0.005)
            theLeg.SetY1(theLeg.GetY1NDC())
            fPads1.cd()
            if j==0 or j==9:
                  theLeg.Draw()
            title = TLatex(0.76,0.93,"DL prediction vs reconstruction, epochs="+Epochs)
            title.SetNDC()
            title.SetTextSize(0.05)
            title.SetTextFont(42)
            title.SetTextAlign(31)
            title.SetLineWidth(2)
          #  title.Draw()
            # Draw p_value
            title2 = TLatex(0.31,0.72,"#chi^{2} p-val = %.2f"%p_value)
            title2.SetNDC()
            title2.SetTextSize(0.04)
            title2.SetTextFont(42)
            title2.SetTextAlign(31)
            title2.SetLineWidth(2)
            if p_value>=0.05:
                   title2.Draw()
            fPads2.cd()
            h_Ratio=TH1D("h_Ratio",""+";%s;%s"%(xtitle,ytitle),nbin,min,max)
            h_Ratio.SetLineColor(1)
            h_Ratio.SetLineWidth(2)
            h_Ratio.SetMarkerColor(1)
            h_Ratio.SetMarkerStyle(8)
            h_Ratio.SetMarkerSize(0.7)
            h_Ratio.SetStats(0)
            h_Ratio.GetYaxis().SetRangeUser( minR , maxR )
            h_Ratio.GetYaxis().SetNdivisions(504,0)
            h_Ratio.GetYaxis().SetTitle("Ratio")
            h_Ratio.GetYaxis().SetTitleOffset(0.47)
            h_Ratio.GetYaxis().SetTitleSize(0.10)
            h_Ratio.GetYaxis().SetLabelSize(0.11)
            h_Ratio.GetXaxis().SetLabelSize(0.1)
            h_Ratio.GetXaxis().SetTitleOffset(1.0)
            h_Ratio.GetXaxis().SetTitleSize(0.1)

            axis1=TGaxis( min,1,max,1, 0,0,0, "L")
            axis1.SetLineColor(1)
            axis1.SetLineWidth(1)
            for i in range(1,h_Ratio.GetNbinsX()+1,1):
                            D  = h_pre.GetBinContent(i)
                            eD = h_pre.GetBinError(i)
                            if D==0: eD=0.92
                            B  = h_rec.GetBinContent(i)
                            eB = h_rec.GetBinError(i)
                            if B<0.1 and eB>=B :
                                           eB=0.92
                                           Err= 0.
                            if B!=0.:
                                           Err=TMath.Sqrt((eD*eD)/(B*B)+(D*D*eB*eB)/(B*B*B*B))
                                           h_Ratio.SetBinContent(i, D/B)
                                           h_Ratio.SetBinError(i, Err)
                            if B==0.:
                                           Err=TMath.Sqrt( (eD*eD)/(eB*eB)+(D*D*eB*eB)/(eB*eB*eB*eB) )
                                           h_Ratio.SetBinContent(i, D/0.92)
                                           h_Ratio.SetBinError(i, Err)
                            if D==0 and B==0:
                                           h_Ratio.SetBinContent(i, -10)
                                           h_Ratio.SetBinError(i, 0)
                            if h_Ratio.GetBinContent(i)>maxR:
                                           h_Ratio.SetBinContent(i, maxR)

            h_Ratio.Draw("e0")
            axis1.Draw()
            c.SaveAs("Plots/"+(branch_rec[j+21])[:-4]+"_comparision.png")
            
            '''
            #2D plots Pre vs Rec
            c2 = TCanvas("c2","c2", 700,700)
            fPads3 = TPad("pad3", "Run2", 0.0, 0.0, 1.00, 1.00)
            fPads3.SetBottomMargin( 0.09)
            fPads3.SetLeftMargin(   0.10)
            fPads3.SetRightMargin(  0.15)
            fPads3.Draw()
            fPads3.cd()
            xtitle = branch_rec[j+21]+" (Rec)"
            ytitle = branch_rec[j+21]+" (Pre)"
            h_2D=TH2D("h_2D",""+";%s;%s"%(xtitle,ytitle),nbin,min,max,nbin,min,max)
            h_2D.Sumw2()
            h_2D.SetStats(0)
            for i in range(y_test.shape[0]):
                      h_2D.Fill(y_test[i][j]-MC_test[i][j],x_decoded[i][j]-MC_test[i][j])
            h_2D.GetYaxis().SetTitleSize(0.04)
            h_2D.GetYaxis().SetTitleOffset(1.20)
         #   h_2D = UnderOverFlow2D(h_2D)
            h_2D.Draw("colz")
            title = TLatex(0.83,0.93,"DL prediction vs reconstruction, epochs="+Epochs)
            title.SetNDC()
            title.SetTextSize(0.04)
            title.SetTextFont(42)
            title.SetTextAlign(31)
            title.SetLineWidth(2)
            #title.Draw()
            c2.SaveAs("Plots/2D_"+branch_rec[j+21]+"_Pre_vs_Rec_comparision.png")
            '''

        # Draw 21 Regression variables:
        for j in range(0, nvariable+12):
            nbin=50
            min=-3.
            max=-min

            if j<9:
               min=min*(Std[j]+epsilon)/beta+Mean[j]
               max=max*(Std[j]+epsilon)/beta+Mean[j]
               Le=max-min
               Av=(max+min)/2.
               min=Av-(Le/2.)*2
               max=Av+(Le/2.)*2
               if j==6:
                    min=0
                    max=5              
         
            if j==9:
                 min=0.
                 max=600.
            if j==10:
                 min=0.
                 max=600.
            if j==11:
                 min=0.
                 max=200.
            if j==12 or j==15:
                 min=-4.
                 max=4.
            if j==13 or j==16 or j==20:
                 min=-4.
                 max=4.
            if j==14:
                 min=0.
                 max=200.
            if j==17:
                 min=0.
                 max=600.
            if j==18:
                 min=-5.
                 max=55.
            if j==19:
                 min=0.
                 max=200.

            lbin=str(round((max-min)/nbin,3))              
            c = TCanvas("c","c", 700,700)
            fPads1 = TPad("pad1", "Run2", 0.0, 0.39, 1.00, 1.00)
            fPads2 = TPad("pad2", "", 0.00, 0.00, 1.00, 0.39)
            fPads1.SetTopMargin(  0.15)
            fPads1.SetBottomMargin( 0.007)
            fPads1.SetLeftMargin(   0.10)
            fPads1.SetRightMargin(  0.03)
            fPads2.SetLeftMargin(   0.10 )
            fPads2.SetRightMargin(  0.03) 
            fPads2.SetBottomMargin( 0.25)
            fPads1.Draw()
            fPads2.Draw()
            fPads1.cd()
            xtitle=branch_rec[j]
            ytitle="Events/"+lbin
            h_gen=TH1D("h_gen",""+";%s;%s"%(xtitle,ytitle),nbin,min,max)
            h_gen.Sumw2()
            h_rec=TH1D("h_rec",""+";%s;%s"%(xtitle,ytitle),nbin,min,max)
            h_rec.Sumw2()
            h_pre=TH1D("h_pre",""+";%s;%s"%(xtitle,ytitle),nbin,min,max)  
            h_pre.Sumw2()

            for v in range(1,N_model):
                      exec("h_pre_"+str(v)+"=TH1D('','',nbin,min,max)")
                      eval("h_pre_"+str(v)).Sumw2()
                      for i in range(eval("x_decoded_"+str(v)).shape[0]):
                               eval("h_pre_"+str(v)).Fill(eval("x_decoded_"+str(v))[i][j])
                    #  exec("h_pre"+str(v)+"=UnderOverFlow1D(h_pre_"+str(v)+")")            

            for i in range(y_test.shape[0]):
                      h_gen.Fill(MC_test[i][j])
                      h_rec.Fill(y_test[i][j])
                      h_pre.Fill(x_decoded[i][j])
                                  
        #    h_gen=UnderOverFlow1D(h_gen)
        #    h_rec=UnderOverFlow1D(h_rec)
        #    h_pre=UnderOverFlow1D(h_pre)

            for v in range(1,N_model):
                   eval("h_pre_"+str(v)).Scale(h_pre.Integral()/eval("h_pre_"+str(v)).Integral());

            x = [];
            y = [];
            eyl = [];
            eyh = [];
            
            for v in range(1,nbin+1):
                       x.append((max-min)*(v-0.5)/nbin+min);
                       y.append(h_pre.GetBinContent(v));
                       ar=[];
                       ar.append(h_pre.GetBinContent(v));
                       for vv in range(1,N_model):
                                ar.append(eval("h_pre_"+str(vv)).GetBinContent(v));
                       ar = sorted(ar)
                       eyl.append(h_pre.GetBinContent(v)-ar[0])
                       eyh.append(ar[len(ar)-1]-h_pre.GetBinContent(v))

           
            for v in range(1,nbin+1):
                       rms=0
                       for vv in range(1,N_model):
                              rms=rms+pow((h_pre.GetBinContent(v)-eval("h_pre_"+str(vv)).GetBinContent(v)),2)  
                       if N_model>1:  
                              rms=math.sqrt(rms/(N_model-1))
                       else:
                              rms=0
                       h_pre.SetBinError(v,math.sqrt(rms*rms+h_pre.GetBinError(v)*h_pre.GetBinError(v)))        

            #CHI2 test
            NBIN=30
            MIN=min
            MAX=max
            
            # log_J1_M
            if j==6:
                              MIN=1
                              MAX=3
            # ST
            if j==9:
                              MIN=75
            # MT
            if j==10:
                              MIN=75
            # Mu_pt
            if j==11:
                              MIN=25
            # Mu_eta
            if j==12:
                              MIN=-2.3
                              MAX=2.3
            # Mu_phi
            if j==13:
                              MIN=-3.1
                              MAX=-3.1
            # J1_pt
            if j==14:
                              MIN=31
                              MAX=-3.1
            # J1_eta
            if j==15:
                              MIN=-2.3
                              MAX=2.3
            # J1_phi
            if j==16:
                              MIN=-3.1
                              MAX=-3.1
            # J1_E
            if j==17:
                              MIN=40
            # J1_M
            if j==18:
                              MIN=2
            # MET
            if j==19:
                              MIN=5
            # MET_phi
            if j==20:
                              MIN=-3.1
                              MAX=-3.1

            h_REC=TH1D("h_rec",""+";%s;%s"%(xtitle,ytitle),NBIN,MIN,MAX)
            h_REC.Sumw2()
            h_PRE=TH1D("h_pre",""+";%s;%s"%(xtitle,ytitle),NBIN,MIN,MAX)
            h_PRE.Sumw2()
            for i in range(y_test.shape[0]):
                      h_REC.Fill(y_test[i][j])
                      h_PRE.Fill(x_decoded[i][j])
         #   p_value=h_REC.Chi2Test(h_PRE,"UF,OF")
            p_value=h_REC.Chi2Test(h_PRE,"UF,OF")
            if p_value>0.99:
                 p_value=1.-float("%.2g" % (1.-p_value))
            else:
                 p_value=round(p_value,3)
         #   p_value=str(p_value)

            if j==6 or j==18:
                   maxR = 2
            else:
                   maxR = 2
            maxY = TMath.Max( h_rec.GetMaximum(), h_pre.GetMaximum() ) 
            maxY = TMath.Max( maxY, h_gen.GetMaximum() )
            h_gen.SetLineColor(4)
            h_gen.SetFillStyle(0)
            h_gen.SetLineWidth(2)
            h_gen.SetLineStyle(1)
            h_rec.SetLineColor(2)
            h_rec.SetFillStyle(0)
            h_rec.SetLineWidth(2)
            h_rec.SetLineStyle(1)
            h_pre.SetLineColor(3) 
            h_pre.SetFillStyle(0)
          #  h_pre.SetBinError(25,1,5)
            h_pre.SetLineWidth(2)
            h_pre.SetLineStyle(1) 
            h_gen.SetStats(0)  
            h_rec.SetStats(0)
            h_pre.SetStats(0)
            h_gen.GetYaxis().SetRangeUser( 0 , maxY*1.1 )
            h_gen.GetYaxis().SetTitleSize(0.06)
            h_gen.GetYaxis().SetTitleOffset(0.82)
            h_gen.Draw("e HIST")
            h_rec.Draw("same e HIST")
            h_pre.Draw("same e HIST")

            gr = TGraphAsymmErrors();
            gr.SetMarkerStyle(21);
            gr.SetMarkerColor(kOrange);
            gr.SetMarkerSize(0.0);
            gr.SetLineWidth(2);
            gr.SetLineColor(kOrange);

            for v in range(1,nbin+1):
                   gr.SetPoint(v,x[v-1],y[v-1])           
                   gr.SetPointEYlow(v,eyl[v-1])
                   gr.SetPointEYhigh(v,eyh[v-1])
       #     gr.Draw("p");

            theLeg = TLegend(0.68, 0.45, 0.99, 0.82, "", "NDC")
            theLeg.SetName("theLegend")
            theLeg.SetBorderSize(0)
            theLeg.SetLineColor(0)
            theLeg.SetFillColor(0)
            theLeg.SetFillStyle(0)
            theLeg.SetLineWidth(0)
            theLeg.SetLineStyle(0)
            theLeg.SetTextFont(42)
            theLeg.SetTextSize(.05)
            theLeg.AddEntry(h_gen,"Generation","L")
            theLeg.AddEntry(h_rec,"Reconstruction","L")
            theLeg.AddEntry(h_pre,"DL Prediction","L")
            theLeg.SetY1NDC(0.9-0.05*6-0.005)
            theLeg.SetY1(theLeg.GetY1NDC())
            fPads1.cd()
            if j==0 or j==9:
                  theLeg.Draw()
            title = TLatex(0.76,0.93,"DL prediction vs reconstruction, epochs="+Epochs)
            title.SetNDC()
            title.SetTextSize(0.05)  
            title.SetTextFont(42)
            title.SetTextAlign(31)  
            title.SetLineWidth(2)
            #title.Draw()
            # Draw p_value
            title2 = TLatex(0.31,0.72,"#chi^{2} p-val = %.2f"%p_value)
            title2.SetNDC()
            title2.SetTextSize(0.04)
            title2.SetTextFont(42)
            title2.SetTextAlign(31)
            title2.SetLineWidth(2)
            if p_value>=0.05:
                   title2.Draw()
            fPads2.cd()
            h_Ratio=TH1D("h_Ratio",""+";%s;%s"%(xtitle,ytitle),nbin,min,max)
            h_Ratio.SetLineColor(1)
            h_Ratio.SetLineWidth(2)
            h_Ratio.SetMarkerColor(1)
            h_Ratio.SetMarkerStyle(8)
            h_Ratio.SetMarkerSize(0.7)
            if j==6 or j==18:
                   h_Ratio.GetYaxis().SetRangeUser( 0 , 2 )
            else:
                   h_Ratio.GetYaxis().SetRangeUser( 0 , 2 )

            h_Ratio.GetYaxis().SetNdivisions(504,0)
            h_Ratio.GetYaxis().SetTitle("Pred/Reco")
            h_Ratio.GetYaxis().SetTitleOffset(0.47)
            h_Ratio.GetYaxis().SetTitleSize(0.10)
            h_Ratio.GetYaxis().SetLabelSize(0.11)
            h_Ratio.GetXaxis().SetLabelSize(0.1)
            h_Ratio.GetXaxis().SetTitleOffset(1.0)
            h_Ratio.GetXaxis().SetTitleSize(0.1)
            h_Ratio.SetStats(0)
            axis1=TGaxis( min,1,max,1, 0,0,0, "L")
            axis1.SetLineColor(1)
            axis1.SetLineWidth(1)
            for i in range(1,h_Ratio.GetNbinsX()+1,1):
                            D  = h_pre.GetBinContent(i)
                            eD = h_pre.GetBinError(i)
                            if D==0: eD=0.92
                            B  = h_rec.GetBinContent(i)
                            eB = h_rec.GetBinError(i)
                            if B<0.1 and eB>=B :
                                           eB=0.92
                                           Err= 0.
                            if B!=0.: 
                                           Err=TMath.Sqrt((eD*eD)/(B*B)+(D*D*eB*eB)/(B*B*B*B))
                                           h_Ratio.SetBinContent(i, D/B)
                                           h_Ratio.SetBinError(i, Err)
                            if B==0.:
                                           Err=TMath.Sqrt( (eD*eD)/(eB*eB)+(D*D*eB*eB)/(eB*eB*eB*eB) )
                                           h_Ratio.SetBinContent(i, D/0.92)
                                           h_Ratio.SetBinError(i, Err)
                            if D==0 and B==0:  
                                           h_Ratio.SetBinContent(i, -10)
                                           h_Ratio.SetBinError(i, 0)
                            if h_Ratio.GetBinContent(i)>maxR:
                                           h_Ratio.SetBinContent(i, maxR)
    
            axis1=TGaxis( min,1,max,1, 0,0,0, "L")
            axis1.SetLineColor(1)
            axis1.SetLineWidth(1)
    
            h_Ratio.Draw("e0")
            axis1.Draw()
    
            c.SaveAs("Plots/"+branch_rec[j]+"_comparision.png")
            
            #2D plots Pre vs Rec
            c2 = TCanvas("c2","c2", 700,700)
            fPads3 = TPad("pad3", "Run2", 0.0, 0.0, 1.00, 1.00)
            fPads3.SetBottomMargin( 0.09)
            fPads3.SetLeftMargin(   0.10)
            fPads3.SetRightMargin(  0.15)
            fPads3.Draw()
            fPads3.cd()
            xtitle = branch_rec[j]+" (Rec)"
            ytitle = branch_rec[j]+" (Pre)"
            h_2D=TH2D("h_2D",""+";%s;%s"%(xtitle,ytitle),nbin,min,max,nbin,min,max)
            h_2D.Sumw2()
            h_2D.SetStats(0)
            for i in range(y_test.shape[0]):
                      h_2D.Fill(y_test[i][j],x_decoded[i][j])
            h_2D.GetYaxis().SetTitleSize(0.04)
            h_2D.GetYaxis().SetTitleOffset(1.20)
        #    h_2D = UnderOverFlow2D(h_2D)
            h_2D.Draw("colz")
            title = TLatex(0.83,0.93,"DL prediction vs reconstruction, epochs="+Epochs)
            title.SetNDC()
            title.SetTextSize(0.04)
            title.SetTextFont(42)
            title.SetTextAlign(31)
            title.SetLineWidth(2)
            #title.Draw()
            c2.SaveAs("Plots/2D_"+branch_rec[j]+"_Pre_vs_Rec_comparision.png")
    
            #2D plots Gen vs Rec
            c3 = TCanvas("c3","c3", 700,700)
            fPads4 = TPad("pad4", "Run2", 0.0, 0.0, 1.00, 1.00)
            fPads4.SetBottomMargin( 0.09)
            fPads4.SetLeftMargin(   0.10)
            fPads4.SetRightMargin(  0.15)
            fPads4.Draw()
            fPads4.cd()
            xtitle = branch_rec[j]+" (Rec)"
            ytitle = branch_rec[j]+" (Gen)"
            h_2D_2=TH2D("h_2D_2",""+";%s;%s"%(xtitle,ytitle),nbin,min,max,nbin,min,max)
            h_2D_2.Sumw2()
            h_2D_2.SetStats(0)
            for i in range(np.array(MC_test).shape[0]):
            #          h_2D_2.Fill(rec[i][j],MC[i][j])
                      h_2D_2.Fill(y_test[i][j],MC_test[i][j])
            h_2D_2.GetYaxis().SetTitleSize(0.04)
            h_2D_2.GetYaxis().SetTitleOffset(1.20)
          #  h_2D_2 = UnderOverFlow2D(h_2D_2)
            h_2D_2.Draw("colz")
            title = TLatex(0.80,0.93,"Generation vs reconstruction, epochs="+Epochs)
            title.SetNDC()
            title.SetTextSize(0.04)
            title.SetTextFont(42)
            title.SetTextAlign(31)
            title.SetLineWidth(2)
            #title.Draw()
            c3.SaveAs("Plots/2D_"+branch_rec[j]+"_Gen_vs_Rec_comparision.png")
           
            #2D plots Gen vs Pre
            c4 = TCanvas("c4","c4", 700,700)
            fPads5 = TPad("pad5", "Run2", 0.0, 0.0, 1.00, 1.00)
            fPads5.SetBottomMargin( 0.09)
            fPads5.SetLeftMargin(   0.10)
            fPads5.SetRightMargin(  0.15)
            fPads5.Draw()
            fPads5.cd()
            xtitle = branch_rec[j]+" (Pre)"
            ytitle = branch_rec[j]+" (Gen)"
            h_2D_3=TH2D("h_2D_3",""+";%s;%s"%(xtitle,ytitle),nbin,min,max,nbin,min,max)
            h_2D_3.Sumw2()
            h_2D_3.SetStats(0)
            for i in range(y_test.shape[0]):
                      h_2D_3.Fill(x_decoded[i][j],MC_test[i][j])
            h_2D_3.GetYaxis().SetTitleSize(0.04)
            h_2D_3.GetYaxis().SetTitleOffset(1.20)
        #    h_2D_3 = UnderOverFlow2D(h_2D_3)
            h_2D_3.Draw("colz")
            title = TLatex(0.80,0.93,"Generation vs DL prediction, epochs="+Epochs)
            title.SetNDC()
            title.SetTextSize(0.04)
            title.SetTextFont(42)
            title.SetTextAlign(31)
            title.SetLineWidth(2)
            #title.Draw()
            c4.SaveAs("Plots/2D_"+branch_rec[j]+"_Gen_vs_Pre_comparision.png")
            
        #Draw loss vs epochs
        file=open(log_file)
        os.system("grep val_loss: "+log_file+" > out.log")
        file=open("out.log")
        lines=[]
        loss=[]
        epochs=[]
        val_loss=[]
        for line in file:
                 lines.append(line)
        for line in lines:
                 line=line.split(":")
                 loss.append(float((line[1].split(" "))[1]))
                 if "val_loss" in line[5]:
                       val_loss.append(float((line[6].split(" "))[1]))
                 else:
                       print("-----read val_loss error------")
        for i in range(1,len(loss)+1):
                 epochs.append(i)
    
        c5 = TCanvas("c5","c5", 900,600)
        fPads6 = TPad("pad6", "Run2", 0.0, 0.0, 1.00, 1.00)
        fPads6.SetBottomMargin( 0.08)
        fPads6.SetLeftMargin(   0.15)
        fPads6.SetRightMargin(  0.05)
        fPads6.SetTopMargin(  0.07)
        fPads6.Draw()
        fPads6.cd()
        g1 = TGraph()
     #   for i in range(1500,len(loss)+1):
        for i in range(1,len(loss)+1):
    #    for i in range(1,500):
                g1.SetPoint(i,epochs[i-1],loss[i-1])
        g1.SetLineColor(kBlue)
        g1.SetLineWidth(2)
        g1.SetMarkerColor(kBlue)
        g1.SetMarkerStyle(2)
    
        g2 = TGraph()
      #  for i in range(1500,len(loss)+1):
        for i in range(1,len(loss)+1):
     #   for i in range(1,500):
                g2.SetPoint(i,epochs[i-1],val_loss[i-1])
        g2.SetLineColor(kRed)
        g2.SetLineWidth(2)
        g2.SetMarkerColor(kRed)
        g2.SetMarkerStyle(2)
    
        mg = TMultiGraph()
        mg.Add(g1)
        mg.Add(g2)
        mg.Draw("al")
        mg.SetTitle("Loss vs epochs")
        mg.GetYaxis().SetTitle("Loss")
        mg.GetXaxis().SetTitle("Epochs")
      #  mg.GetXaxis().SetRangeUser(1.5,500)
        mg.GetXaxis().SetRangeUser(1.6,len(loss)-0.5)
      #  mg.GetXaxis().SetRangeUser(1500.,len(loss))
        mg.GetYaxis().SetRangeUser(0.3,0.32)

        theLeg = TLegend(0.48, 0.52, 0.89, 0.89, "", "NDC")
        theLeg.SetName("theLegend")
        theLeg.SetBorderSize(0)
        theLeg.SetLineColor(0)
        theLeg.SetFillColor(0)
        theLeg.SetFillStyle(0)
        theLeg.SetLineWidth(0)
        theLeg.SetLineStyle(0)
        theLeg.SetTextFont(42)
        theLeg.SetTextSize(.05)
        theLeg.AddEntry(g1,"Training loss","L")
        theLeg.AddEntry(g2,"Validation loss","L")
        theLeg.SetY1NDC(0.9-0.05*6-0.005)
        theLeg.SetY1(theLeg.GetY1NDC())
        theLeg.Draw()
    
        c5.SaveAs("Plots/Loss.png")

def UnderOverFlow1D(h):
    Bins=h.GetNbinsX()
    h.SetBinContent( 1,  h.GetBinContent(1)+h.GetBinContent(0) )
    h.SetBinError(   1,  math.sqrt( h.GetBinError(1)*h.GetBinError(1) + h.GetBinError(0)*h.GetBinError(0)) )
    h.SetBinContent( Bins,  h.GetBinContent(Bins)+h.GetBinContent(Bins+1) )
    h.SetBinError(   Bins,  math.sqrt( h.GetBinError(Bins)*h.GetBinError(Bins) + h.GetBinError(Bins+1)*h.GetBinError(Bins+1)) )
    return h

def UnderOverFlow2D(h):
    BinsX=h.GetNbinsX()
    BinsY=h.GetNbinsY()
    for i in range(0,BinsX):
          h.SetBinContent( i+1, 1,  h.GetBinContent(i+1, 1)+h.GetBinContent(i+1, 0) )
          h.SetBinError( i+1, 1,  math.sqrt(h.GetBinError(i+1, 1)*h.GetBinError(i+1, 1)+h.GetBinError(i+1, 0)*h.GetBinError(i+1, 0)) )
          h.SetBinContent( i+1, BinsY,  h.GetBinContent(i+1, BinsY)+h.GetBinContent(i+1, BinsY+1) )
          h.SetBinError( i+1, BinsY,  math.sqrt(h.GetBinError(i+1, BinsY)*h.GetBinError(i+1, BinsY)+h.GetBinError(i+1, BinsY+1)*h.GetBinError(i+1, BinsY+1)) )
          h.SetBinContent( 1, i+1,  h.GetBinContent(1, i+1)+h.GetBinContent(0, i+1) )
          h.SetBinError( 1, i+1,  math.sqrt(h.GetBinError(1, i+1)*h.GetBinError(1, i+1)+h.GetBinError(0, i+1)*h.GetBinError(0, i+1)) )
          h.SetBinContent( BinsX, i+1,  h.GetBinContent(BinsX, i+1)+h.GetBinContent(BinsX+1, i+1) )
          h.SetBinError( BinsX, i+1,  math.sqrt(h.GetBinError(BinsX, i+1)*h.GetBinError(BinsX, i+1)+h.GetBinError(BinsX+1, i+1)*h.GetBinError(BinsX+1, i+1)) )
    h.SetBinContent( 1, 1,  h.GetBinContent(1, 1)+h.GetBinContent(0, 0) )
    h.SetBinError( 1, 1,  math.sqrt(h.GetBinError(1, 1)*h.GetBinError(1, 1)+h.GetBinError(0, 0)*h.GetBinError(0, 0)) )
    h.SetBinContent( BinsX, 1,  h.GetBinContent(BinsX, 1)+h.GetBinContent(BinsX+1, 0) )
    h.SetBinError( BinsX, 1,  math.sqrt(h.GetBinError(BinsX, 1)*h.GetBinError(BinsX, 1)+h.GetBinError(BinsX+1, 0)*h.GetBinError(BinsX+1, 0)) )
    h.SetBinContent( 1, BinsY,  h.GetBinContent(1, BinsY)+h.GetBinContent(0, BinsY+1) )
    h.SetBinError( 1, BinsY,  math.sqrt(h.GetBinError(1, BinsY)*h.GetBinError(1, BinsY)+h.GetBinError(0, BinsY+1)*h.GetBinError(0, BinsY+1)) )
    h.SetBinContent( BinsX, BinsY,  h.GetBinContent(BinsX, BinsY)+h.GetBinContent(BinsX+1, BinsY+1) )
    h.SetBinError( BinsX, BinsY,  math.sqrt(h.GetBinError(BinsX, BinsY)*h.GetBinError(BinsX, BinsY)+h.GetBinError(BinsX+1, BinsY+1)*h.GetBinError(BinsX+1, BinsY+1)) )
    return h

def array2D_float(array):
    for i in range(0,array.shape[0]):
           for j in range(0,array.shape[1]):
                    array[i][j]=array[i][j][0]
    return array

def quantile_loss(target, pred):
    alpha = 0.841 #0.66
    err = target - pred
    return K.tf.where(err>=0, alpha*err, (alpha-1)*err)

def mean_std_cut(arr, r):
    arr=arr.tolist()
    arr = sorted(arr)
    arr = arr[int(r*len(arr)):int((1-r)*len(arr))]
    arr = K.constant(arr)
    mean = K.mean(arr)
    std = K.std(arr)
    return [mean,std]

def BN(arr):
    mean = K.mean(arr, axis=0, keepdims=True)
    std = K.std(arr, axis=0, keepdims=True)
    arr_normed=(arr-mean)*beta/(std+epsilon)
    return [arr_normed, mean, std]

def pxpypz(arr):
        arr2=arr.tolist()
        P_K=TLorentzVector()
        P_Pi=TLorentzVector()
        P_Pis=TLorentzVector()
        P_Mu=TLorentzVector()
        for i in range(0, arr.shape[0]):
              P_K.SetPtEtaPhiM(arr[i][2],arr[i][0],arr[i][1],M_K)
              P_Pi.SetPtEtaPhiM(arr[i][5],arr[i][3],arr[i][4],M_Pi)
              P_Pis.SetPtEtaPhiM(arr[i][8],arr[i][6],arr[i][7],M_Pi)
              P_Mu.SetPtEtaPhiM(arr[i][11],arr[i][9],arr[i][10],M_Mu)
              arr2[i][0]=P_K.Px()
              arr2[i][1]=P_K.Py()
              arr2[i][2]=P_K.Pz()
              arr2[i][3]=P_Pi.Px()
              arr2[i][4]=P_Pi.Py()
              arr2[i][5]=P_Pi.Pz()
              arr2[i][6]=P_Pis.Px()
              arr2[i][7]=P_Pis.Py()
              arr2[i][8]=P_Pis.Pz()
              arr2[i][9]=P_Mu.Px()
              arr2[i][10]=P_Mu.Py()
              arr2[i][11]=P_Mu.Pz()
        return np.array(arr2)

def remove_num(arr, r):
    arr=np.array(arr)
    n=arr.shape[0]
    a=np.arange(n)
    arr=np.column_stack((arr, a))
    arr = sorted(arr,key=lambda s: s[0])
    m=int(n*r)
    num=[]
    for i in range(0,m):
         num.append(arr[i][1])
    for i in range(n-m,n):
         num.append(arr[i][1])
    return num

def DR(eta1,phi1,eta2,phi2):
    dphi = Pi-abs(abs(phi1-phi2)-Pi)
    deta = eta1-eta2
    dr = math.sqrt(dphi*dphi+deta*deta)
    return dr             

def get_lr_metric(optimizer):

        def lr(y_true, y_pred):

                return optimizer.lr

        return lr

if __name__ == '__main__':              
    train_and_apply()
