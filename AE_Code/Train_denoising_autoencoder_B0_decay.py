from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import keras
from keras.layers import Activation, Dense, Input
from keras.layers import Conv2D, Flatten
from keras.layers import Reshape, Conv2DTranspose
from keras.layers.advanced_activations import LeakyReLU
from keras.layers import Lambda
from keras.models import Model
from keras.models import load_model
from keras.models import save_model
from keras import backend as K
from keras.optimizers import Adam
from keras.backend import random_normal
from keras.callbacks import ReduceLROnPlateau
from keras.datasets import mnist
from keras.callbacks import ModelCheckpoint
from keras.callbacks import EarlyStopping
from keras.callbacks import Callback
from keras.callbacks import LearningRateScheduler
from keras.utils import plot_model
import numpy as np
from sklearn.model_selection import train_test_split 
import matplotlib.pyplot as plt
import sys
import subprocess
from subprocess import Popen 
from optparse   import OptionParser
from time       import gmtime, strftime
from array import array 
from root_numpy import root2array, array2tree, rec2array, fill_hist
import ROOT
import math
import random
import uproot
import os
from ROOT import gROOT, TLorentzVector, TPaveLabel, TPie, gStyle, gSystem, TGaxis, TStyle, TLatex, TString, TF1,TFile,TLine, TLegend, TH1D,TH2D,THStack, TGraph,TMultiGraph, TGraphErrors,TChain, TCanvas, TMatrixDSym, TMath, TText, TPad, TVectorD, RooFit, RooArgSet, RooArgList, RooArgSet, RooAbsData, RooAbsPdf, RooAddPdf, RooWorkspace, RooExtendPdf,RooCBShape, RooLandau, RooFFTConvPdf, RooGaussian, RooBifurGauss, RooArgusBG,RooDataSet, RooExponential,RooBreitWigner, RooVoigtian, RooNovosibirsk, RooRealVar,RooFormulaVar, RooDataHist, RooHist,RooCategory, RooChebychev, RooSimultaneous, RooGenericPdf,RooConstVar, RooKeysPdf, RooHistPdf, RooEffProd, RooProdPdf, TIter, kTRUE, kFALSE, kGray, kRed, kDashed, kGreen,kAzure, kOrange, kBlack,kBlue,kYellow,kCyan, kMagenta, kWhite

parser = OptionParser()
parser.add_option('--maxevents',action="store",type="int",dest="maxevents",default=1000000)
parser.add_option('--epochs',action="store",type="int",dest="epochs",default=1)
parser.add_option('--train_apply',action="store",type="int",dest="train_apply",default=1)
(options, args) = parser.parse_args()

Pi=3.1415926535898 
M_Mu=0.10565
M_Pi=0.13957
M_K=0.49367
M_D0=1.86483
M_Dst=2.01026
M_B0=5.27963
Mean_pre=[]
Std_pre=[]
Mean=[]
Std=[]
gamma=0
beta=1.#0.15
epsilon=K.epsilon()
weight0=0.
weight_end=1.
epoch0=500
epoch_rise=1000
#epoch_rise2=1500
factor=(weight_end-weight0)/(epoch_rise-epoch0)
#factor2=(weight_end-weight0)/(epoch_rise2-epoch_rise)
#scale=0.1
Range=1.
Range2=5.
weight_mqe=1.
weight_m=weight_mqe
weight_q=weight_mqe
weight_e=weight_mqe
sqrt_weight_m=math.sqrt(weight_m)
sqrt_weight_q=math.sqrt(weight_q)
sqrt_weight_e=math.sqrt(weight_e)
initial_lrate = 0.001
lr_decay=1.#0.2
#lr_decay2=0.4

class MyCallback(Callback):
    def __init__(self, weight1, weight2, weight3):
         self.weight1 = weight1
         self.weight2 = weight2
         self.weight3 = weight3
    def on_epoch_end(self, epoch, logs={}):
    #     if epoch>=epoch0 and epoch<epoch_rise:
     #          K.set_value(self.weight3, K.get_value(self.weight3)+factor)
    #     if epoch>=epoch_rise and epoch<epoch_rise2:
         print("weight")
         print(K.get_value(self.weight1))
         print(K.get_value(self.weight2))
         print(K.get_value(self.weight3))

def scheduler(epoch):
    lr = initial_lrate
#    if epoch>(epoch0-400): # and epoch<=(epoch_rise-10):
    lr = initial_lrate/(1.+lr_decay*epoch)
  #  if epoch>(epoch_rise-10):
   #          lr = initial_lrate/(lr_decay2*epoch)
    print("lr")
    print(lr)
    return lr

def comput(inputs):
    # Inputs are true values of prediction for REC
    x = inputs
    x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14=K.tf.split(x,14,axis=1,num=None)

    Px_K=x3*K.cos(x2)
    Py_K=x3*K.sin(x2)
    Pz_K=x3*(K.exp(x1)-K.exp(-x1))/2.0
    Px_Pi=x6*K.cos(x5)
    Py_Pi=x6*K.sin(x5)
    Pz_Pi=x6*(K.exp(x4)-K.exp(-x4))/2.0
    Px_Pis=x9*K.cos(x8)
    Py_Pis=x9*K.sin(x8)
    Pz_Pis=x9*(K.exp(x7)-K.exp(-x7))/2.0
    Px_Mu=x12*K.cos(x11)
    Py_Mu=x12*K.sin(x11)
    Pz_Mu=x12*(K.exp(x10)-K.exp(-x10))/2.0
    Eta_B0=x13
    Phi_B0=x14

    E_K=K.sqrt(K.relu(Px_K*Px_K+Py_K*Py_K+Pz_K*Pz_K+M_K*M_K))
    E_Pi=K.sqrt(K.relu(Px_Pi*Px_Pi+Py_Pi*Py_Pi+Pz_Pi*Pz_Pi+M_Pi*M_Pi))
    E_Pis=K.sqrt(K.relu(Px_Pis*Px_Pis+Py_Pis*Py_Pis+Pz_Pis*Pz_Pis+M_Pi*M_Pi))
    E_Mu=K.sqrt(K.relu(Px_Mu*Px_Mu+Py_Mu*Py_Mu+Pz_Mu*Pz_Mu+M_Mu*M_Mu))
    E_Vis=E_K+E_Pi+E_Pis+E_Mu

    Px_D0=Px_K+Px_Pi
    Py_D0=Py_K+Py_Pi
    Pz_D0=Pz_K+Pz_Pi
    Pt_D0=K.sqrt(K.relu(Px_D0*Px_D0+Py_D0*Py_D0))
    P_D0=K.sqrt(K.relu(Px_D0*Px_D0+Py_D0*Py_D0+Pz_D0*Pz_D0))
    costheta=Pz_D0/(K.relu(P_D0)+epsilon)
    sintheta=Pt_D0/(K.relu(P_D0)+epsilon)
    tan_half_theta=(1-costheta)/(K.relu(sintheta)+epsilon)
    E_D0=E_K+E_Pi

    Eta_D0=-K.tf.math.log(K.relu(tan_half_theta)+epsilon)
    Phi_D0=K.tf.math.atan(Py_D0/(K.abs(Px_D0)+epsilon))
    sign_y=(Py_D0+epsilon)/(K.abs(Py_D0)+epsilon)  # 1 or -1
    sign_x=(Px_D0+epsilon)/(K.abs(Px_D0)+epsilon)
    Phi_D0=Phi_D0*sign_x+Pi*sign_y*(sign_x-1.)/(-2.)

    Px_Dst=Px_K+Px_Pi+Px_Pis
    Py_Dst=Py_K+Py_Pi+Py_Pis
    Pz_Dst=Pz_K+Pz_Pi+Pz_Pis
    Pt_Dst=K.sqrt(K.relu(Px_Dst*Px_Dst+Py_Dst*Py_Dst))
    P_Dst=K.sqrt(K.relu(Px_Dst*Px_Dst+Py_Dst*Py_Dst+Pz_Dst*Pz_Dst))
    costheta=Pz_Dst/(K.relu(P_Dst)+epsilon)
    sintheta=Pt_Dst/(K.relu(P_Dst)+epsilon)
    tan_half_theta=(1-costheta)/(K.relu(sintheta)+epsilon)
    E_Dst=E_K+E_Pi+E_Pis

    Eta_Dst=-K.tf.math.log(K.relu(tan_half_theta)+epsilon)
    Phi_Dst=K.tf.math.atan(Py_Dst/(K.abs(Px_Dst)+epsilon))
    sign_y=(Py_Dst+epsilon)/(K.abs(Py_Dst)+epsilon)  # 1 or -1
    sign_x=(Px_Dst+epsilon)/(K.abs(Px_Dst)+epsilon)
    Phi_Dst=Phi_Dst*sign_x+Pi*sign_y*(sign_x-1.)/(-2.)

    Px_Vis=Px_K+Px_Pi+Px_Pis+Px_Mu
    Py_Vis=Py_K+Py_Pi+Py_Pis+Py_Mu
    Pz_Vis=Pz_K+Pz_Pi+Pz_Pis+Pz_Mu
    Pt_Vis=K.sqrt(K.relu(Px_Vis*Px_Vis+Py_Vis*Py_Vis))
    M_Vis=K.sqrt(K.relu(E_Vis*E_Vis-Pt_Vis*Pt_Vis-Pz_Vis*Pz_Vis))
   
    Pt_B0=Pt_Vis*M_B0/(K.relu(M_Vis)+epsilon)
    Px_B0=Pt_B0*K.cos(Phi_B0)
    Py_B0=Pt_B0*K.sin(Phi_B0)
    Pz_B0=Pt_B0*(K.exp(Eta_B0)-K.exp(-Eta_B0))/2.0
    E_B0=K.sqrt(K.relu(Px_B0*Px_B0+Py_B0*Py_B0+Pz_B0*Pz_B0+M_B0*M_B0))

    M2_miss=(E_B0-E_K-E_Pi-E_Pis-E_Mu)*(E_B0-E_K-E_Pi-E_Pis-E_Mu)-(Px_B0-Px_K-Px_Pi-Px_Pis-Px_Mu)*(Px_B0-Px_K-Px_Pi-Px_Pis-Px_Mu)-(Py_B0-Py_K-Py_Pi-Py_Pis-Py_Mu)*(Py_B0-Py_K-Py_Pi-Py_Pis-Py_Mu)-(Pz_B0-Pz_K-Pz_Pi-Pz_Pis-Pz_Mu)*(Pz_B0-Pz_K-Pz_Pi-Pz_Pis-Pz_Mu)
    q2=(E_B0-E_K-E_Pi-E_Pis)*(E_B0-E_K-E_Pi-E_Pis)-(Px_B0-Px_K-Px_Pi-Px_Pis)*(Px_B0-Px_K-Px_Pi-Px_Pis)-(Py_B0-Py_K-Py_Pi-Py_Pis)*(Py_B0-Py_K-Py_Pi-Py_Pis)-(Pz_B0-Pz_K-Pz_Pi-Pz_Pis)*(Pz_B0-Pz_K-Pz_Pi-Pz_Pis)

    # Comput Est_mu
    # Revolve coordinate to make P_B0 and X-axis coincide.
    sintheta=2.*K.exp(-Eta_B0)/(1.+K.exp(-2.*Eta_B0))
    costheta=(1.-K.exp(-2.*Eta_B0))/(1.+K.exp(-2.*Eta_B0))
    sinphi=K.sin(Phi_B0)
    cosphi=K.cos(Phi_B0)
    P_Mu=K.sqrt(K.relu(Px_Mu*Px_Mu+Py_Mu*Py_Mu+Pz_Mu*Pz_Mu))
    P_B0=K.sqrt(K.relu(Px_B0*Px_B0+Py_B0*Py_B0+Pz_B0*Pz_B0))
    Px_Mu2=Px_Mu*cosphi*sintheta+Py_Mu*sinphi*sintheta+Pz_Mu*costheta
    Py_Mu2=-Px_Mu*sinphi+Py_Mu*cosphi
    Pz_Mu2=-Px_Mu*cosphi*costheta-Py_Mu*sinphi*costheta+Pz_Mu*sintheta
    Beta_Mu=1./K.sqrt(1.+K.relu((M_Mu/(K.relu(P_Mu)+epsilon))*(M_Mu/(K.relu(P_Mu)+epsilon))))
    Beta_B0=1./K.sqrt(1.+K.relu((M_B0/(K.relu(P_B0)+epsilon))*(M_B0/(K.relu(P_B0)+epsilon))))
    Betax_Mu2=Beta_Mu*Px_Mu2/(K.relu(P_Mu)+epsilon)
    Betay_Mu2=Beta_Mu*Py_Mu2/(K.relu(P_Mu)+epsilon)
    Betaz_Mu2=Beta_Mu*Pz_Mu2/(K.relu(P_Mu)+epsilon)

    # Lorentz Transformation
    Betax_Mu3=(Betax_Mu2-Beta_B0)/(K.relu(1-Beta_B0*Betax_Mu2)+epsilon)
    Betay_Mu3=K.sqrt(K.relu(1-Beta_B0*Beta_B0))*Betay_Mu2/(K.relu(1-Beta_B0*Betax_Mu2)+epsilon)
    Betaz_Mu3=K.sqrt(K.relu(1-Beta_B0*Beta_B0))*Betaz_Mu2/(K.relu(1-Beta_B0*Betax_Mu2)+epsilon)
    Beta_Mu3=K.sqrt(K.relu(Betax_Mu3*Betax_Mu3+Betay_Mu3*Betay_Mu3+Betaz_Mu3*Betaz_Mu3))
    P_Mu3=M_Mu*Beta_Mu3/K.sqrt(K.relu(1-Beta_Mu3*Beta_Mu3)+epsilon)
    Est_mu=K.sqrt(K.relu(P_Mu3*P_Mu3+M_Mu*M_Mu))

    return K.concatenate([E_D0, Eta_D0, Phi_D0, Pt_D0, E_Dst, Eta_Dst, Phi_Dst, Pt_Dst, Pt_B0, M2_miss, q2, Est_mu], axis=1) 

def Comput_BN(inputs):
    x_mae, x_quantile, mc = inputs

    x_mean = x_mae
 
    x_std = K.abs(x_mae-x_quantile)

    x=K.random_normal(shape=K.shape(x_mean), mean=x_mean, stddev=x_std) 

    x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14=K.tf.split(x,14,axis=1,num=None)

    mc1,mc2,mc3,mc4,mc5,mc6,mc7,mc8,mc9,mc10,mc11,mc12,mc13,mc14=K.tf.split(mc,14,axis=1,num=None)

    # Return to true values after BN:

    x1=mc1+(x1-mc1)*Std_pre[0]/(Std[0]*Std_pre[11]/Std[11])
    x2=mc2+(x2-mc2)*Std_pre[1]/(Std[1]*Std_pre[11]/Std[11])
    x3=mc3+(x3-mc3)*Std_pre[2]/(Std[2]*Std_pre[11]/Std[11])
    x4=mc4+(x4-mc4)*Std_pre[3]/(Std[3]*Std_pre[11]/Std[11])
    x5=mc5+(x5-mc5)*Std_pre[4]/(Std[4]*Std_pre[11]/Std[11])
    x6=mc6+(x6-mc6)*Std_pre[5]/(Std[5]*Std_pre[11]/Std[11])
    x7=mc7+(x7-mc7)*Std_pre[6]/(Std[6]*Std_pre[11]/Std[11])
    x8=mc8+(x8-mc8)*Std_pre[7]/(Std[7]*Std_pre[11]/Std[11])
    x9=mc9+(x9-mc9)*Std_pre[8]/(Std[8]*Std_pre[11]/Std[11])
    x10=mc10+(x10-mc10)*Std_pre[9]/(Std[9]*Std_pre[11]/Std[11])
    x11=mc11+(x11-mc11)*Std_pre[10]/(Std[10]*Std_pre[11]/Std[11])
    x12=mc12+(x12-mc12)*Std_pre[11]/(Std[11]*Std_pre[11]/Std[11])
    x13=mc13+(x13-mc13)*Std_pre[12]/(Std[12]*Std_pre[11]/Std[11])
    x14=mc14+(x14-mc14)*Std_pre[13]/(Std[13]*Std_pre[11]/Std[11])

    # Return to true values: pre
    x1=x1*(Std[0]+epsilon)/beta+Mean[0]
    x2=x2*(Std[1]+epsilon)/beta+Mean[1]
    x3=x3*(Std[2]+epsilon)/beta+Mean[2]
    x4=x4*(Std[3]+epsilon)/beta+Mean[3]
    x5=x5*(Std[4]+epsilon)/beta+Mean[4]
    x6=x6*(Std[5]+epsilon)/beta+Mean[5]
    x7=x7*(Std[6]+epsilon)/beta+Mean[6]
    x8=x8*(Std[7]+epsilon)/beta+Mean[7]
    x9=x9*(Std[8]+epsilon)/beta+Mean[8]
    x10=x10*(Std[9]+epsilon)/beta+Mean[9]
    x11=x11*(Std[10]+epsilon)/beta+Mean[10]
    x12=x12*(Std[11]+epsilon)/beta+Mean[11]
    x13=x13*(Std[12]+epsilon)/beta+Mean[12]
    x14=x14*(Std[13]+epsilon)/beta+Mean[13]

    # Eta
    x1=K.clip(x1, -3., 3.)
    x4=K.clip(x4, -3., 3.)
    x7=K.clip(x7, -3., 3.)
    x10=K.clip(x10, -3., 3.)
    x13=K.clip(x13, -3., 3.)

    # Phi
    x2=K.clip(x2, -Pi, Pi)
    x5=K.clip(x5, -Pi, Pi)
    x8=K.clip(x8, -Pi, Pi)
    x11=K.clip(x11, -Pi, Pi)
    x14=K.clip(x14, -Pi, Pi)

    # Pt
    x3=K.relu(x3)
    x6=K.relu(x6)
    x9=K.relu(x9)
    x12=K.relu(x12)

    # Fix values after BN: pre=rec
    x1_fix=(x1-Mean[0])*beta/(Std[0]+epsilon)
    x2_fix=(x2-Mean[1])*beta/(Std[1]+epsilon)
    x3_fix=(x3-Mean[2])*beta/(Std[2]+epsilon)
    x4_fix=(x4-Mean[3])*beta/(Std[3]+epsilon)
    x5_fix=(x5-Mean[4])*beta/(Std[4]+epsilon)
    x6_fix=(x6-Mean[5])*beta/(Std[5]+epsilon)
    x7_fix=(x7-Mean[6])*beta/(Std[6]+epsilon)
    x8_fix=(x8-Mean[7])*beta/(Std[7]+epsilon)
    x9_fix=(x9-Mean[8])*beta/(Std[8]+epsilon)
    x10_fix=(x10-Mean[9])*beta/(Std[9]+epsilon)
    x11_fix=(x11-Mean[10])*beta/(Std[10]+epsilon)
    x12_fix=(x12-Mean[11])*beta/(Std[11]+epsilon)
    x13_fix=(x13-Mean[12])*beta/(Std[12]+epsilon)
    x14_fix=(x14-Mean[13])*beta/(Std[13]+epsilon)

    x_fix=K.concatenate([x1_fix, x2_fix, x3_fix, x4_fix, x5_fix, x6_fix, x7_fix, x8_fix, x9_fix, x10_fix, x11_fix, x12_fix, x13_fix, x14_fix], axis=1)

    inputs=K.concatenate([x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14], axis=1)

    comput_true = comput(inputs)
    x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12=K.tf.split(comput_true,12,axis=1,num=None)

    # Normalization   
    x1=(x1-Mean[14])*beta/(Std[14]+epsilon)
    x2=(x2-Mean[15])*beta/(Std[15]+epsilon)
    x3=(x3-Mean[16])*beta/(Std[16]+epsilon)
    x4=(x4-Mean[17])*beta/(Std[17]+epsilon)
    x5=(x5-Mean[18])*beta/(Std[18]+epsilon)
    x6=(x6-Mean[19])*beta/(Std[19]+epsilon)
    x7=(x7-Mean[20])*beta/(Std[20]+epsilon)
    x8=(x8-Mean[21])*beta/(Std[21]+epsilon)
    x9=(x9-Mean[22])*beta/(Std[22]+epsilon)
    x10=(x10-Mean[23])*beta/(Std[23]+epsilon)
    x11=(x11-Mean[24])*beta/(Std[24]+epsilon)
    x12=(x12-Mean[25])*beta/(Std[25]+epsilon)

    return [K.concatenate([x_fix, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12], axis=1), x_mae, x_quantile]

def train_and_apply():
    np.random.seed(1)
    ROOT.gROOT.SetBatch()

    # Extract data from root file
  #  input_file="test.root"
    input_file="SIM.root"
    tree = uproot.open(input_file)["outA/Tevts"] 
    branch_mc=["MC_K_eta","MC_K_phi","MC_K_pt","MC_pi_eta","MC_pi_phi","MC_pi_pt","MC_pis_eta","MC_pis_phi","MC_pis_pt","MC_mu_eta","MC_mu_phi","MC_mu_pt","MC_B_eta","MC_B_phi"]
    branch_rec=["K_refitpiK_eta","K_refitpiK_phi","K_refitpiK_pt","pi_refitpiK_eta","pi_refitpiK_phi","pi_refitpiK_pt","pis_refitD0pis_eta","pis_refitD0pis_phi","pis_refitD0pis_pt","mu_refitD0pismu_eta","mu_refitD0pismu_phi","mu_refitD0pismu_pt","B_Dstmu_eta","B_Dstmu_phi"]
    nvariable=len(branch_rec)
    for i in range(0,nvariable):
          if i==0: 
                 mc = tree.array(branch_mc[0], entrystop=options.maxevents)
                 rec = tree.array(branch_rec[0], entrystop=options.maxevents)
          else: 
                 mc = np.vstack((mc,tree.array(branch_mc[i], entrystop=options.maxevents)))
                 rec = np.vstack((rec,tree.array(branch_rec[i], entrystop=options.maxevents)))
    mc=mc.T
    rec=rec.T
    rec=array2D_float(rec)
    mc_0=mc
    rec_0=rec
    rec_minus=rec-mc
    # Different type of reconstruction variables

    # BN normalization   
    # 26 true values
    global Mean
    global Std
    # 14 rec-mc values
    global Mean_pre
    global Std_pre

    mc_and_rec = np.vstack((mc, rec))
    ar=np.array(mc_and_rec)
    ar = K.constant(ar)
    a, m, s=BN(ar)
    sess = K.tf.Session()
    mc_and_rec=np.array(sess.run(a))
    for i in range(0,nvariable):
           Mean.append(sess.run(m)[0,i])
           Std.append(sess.run(s)[0,i])

    mc = mc_and_rec[:mc.shape[0],:]
    rec = mc_and_rec[mc.shape[0]:,:]

    # Comput Lambda layer variables for GEN and RECO 

    MC=K.constant(mc_0)
    MC=comput(MC)
    MC=sess.run(MC)

    REC=K.constant(rec_0)
    REC=comput(REC)
    REC=sess.run(REC)

    branch_MC=["MC_D0_eta","MC_D0_phi","MC_D0_pt","MC_Dst_eta","MC_Dst_phi","MC_Dst_pt","MC_B_pt","MC_M2_miss","MC_q2","MC_Est_mu"]
    nvariable_MC=len(branch_MC)
    for i in range(0,nvariable_MC):
          if i==0:
                 mc2 = tree.array(branch_MC[0], entrystop=options.maxevents)
          else:
                 mc2 = np.vstack((mc2,tree.array(branch_MC[i], entrystop=options.maxevents)))
    mc2=mc2.T
    
    MC=np.column_stack((MC[:,0], mc2[:,0:3], MC[:,4], mc2[:,3:10]))

    # Remove bad events
    # Select trgMuon_match_BMuon to be 1
    trgMuon_match_BMuon = tree.array("trgMuon_match_BMuon", entrystop=options.maxevents)

    idelete=[]
    mc_0=np.column_stack((mc_0,MC))
    rec_0=np.column_stack((rec_0,REC))
    for i in range(0,mc_0.shape[0]):
         tag=0
         for j in range(0,mc_0.shape[1]):
                imatch=(trgMuon_match_BMuon[i]==0)
                idiff=(abs(rec_0[i][j]-mc_0[i][j])>5. and (j==1 or j==4 or j==7 or j==10 or j==13 or j==16 or j==20)) # Remove phi issue
                if j<14:
                       ierror=(abs(rec_0[i][j]-mc_0[i][j])/Std[j]>0.2)         # Remove abnormal events
                else: 
                       ierror=0
     #           icut_m=(rec_0[i][j]>50. or rec_0[i][j]<-40.) and j==23
      #          icut_q=(rec_0[i][j]>50. or rec_0[i][j]<-40.) and j==24
       #         icut_e=(rec_0[i][j]>10. or rec_0[i][j]<-5.) and j==25

                icut_m=(rec_0[i][j]>11. or rec_0[i][j]<-6.) and j==23
                icut_q=(rec_0[i][j]>12. or rec_0[i][j]<-3.) and j==24
                icut_e=(rec_0[i][j]>4. or rec_0[i][j]<-1.) and j==25

     #           icut_m=0
      #          icut_q=0
       #         icut_e=0

                if (imatch==1 or idiff==1 or ierror==1 or icut_m==1 or icut_q==1 or icut_e==1) and tag==0:
                        idelete.append(i)
                        tag=1

    mc = np.delete(mc, idelete, axis=0)
    rec = np.delete(rec, idelete, axis=0)
    rec_minus = np.delete(rec_minus, idelete, axis=0)
    MC = np.delete(MC, idelete, axis=0)
    REC = np.delete(REC, idelete, axis=0)

    # Normalization for regression iterms
    # Mean and std for M2_miss, q2 and Est_mu
    r_m=0.
    r_q=0.
    r_e=0.
    mean_m, std_m = sess.run(mean_std_cut(REC[:,9], r_m))
    mean_q, std_q = sess.run(mean_std_cut(REC[:,10], r_q))
    mean_e, std_e = sess.run(mean_std_cut(REC[:,11], r_e))
    std_m=std_m/sqrt_weight_m
    std_q=std_q/sqrt_weight_q
    std_e=std_e/sqrt_weight_e

    # Normalization for M2_miss, q2 and Est_mu
    REC_M2_miss=(REC[:,9]-mean_m)*beta/(std_m+epsilon)
    REC_q2=(REC[:,10]-mean_q)*beta/(std_q+epsilon)
    REC_Est_mu=(REC[:,11]-mean_e)*beta/(std_e+epsilon)

    ar = K.constant(REC)
    a, m, s=BN(ar)

    REC=sess.run(a)
    for i in range(0,REC.shape[1]):
           if i==9:
               Mean.append(mean_m)
               Std.append(std_m)
           elif i==10:
               Mean.append(mean_q)
               Std.append(std_q)
           elif i==11:
               Mean.append(mean_e)
               Std.append(std_e)
           else:
               Mean.append(sess.run(m)[0,i])
               Std.append(sess.run(s)[0,i])

    # Concatenate
    REC=np.column_stack((REC[:,0:9], REC_M2_miss, REC_q2, REC_Est_mu))

    # Force in range [-Range,Range]

    REC2_m=np.clip(REC[:,9], -Range*sqrt_weight_m, Range*sqrt_weight_m)
    REC2_q=np.clip(REC[:,10], -Range*sqrt_weight_q, Range*sqrt_weight_q)
    REC2_e=np.clip(REC[:,11], -Range*sqrt_weight_e, Range*sqrt_weight_e)

    REC2=np.column_stack((REC2_m, REC2_q, REC2_e))

    # BN for MC 12 regression variables 
    for i in range(0, MC.shape[1]):
             MC[:,i]=(MC[:,i]-Mean[i+14])*beta/(Std[i+14]+epsilon)

    MC=np.column_stack((mc,MC))    
    rec=np.column_stack((rec,REC))

    '''
    # Remove abnormal events    
    for i in range(0, rec_minus.shape[1]):
              idelete2=remove_num(rec_minus[:,i], 0.005)
              mc = np.delete(mc, idelete2, axis=0)
              rec = np.delete(rec, idelete2, axis=0)
              rec_minus = np.delete(rec_minus, idelete2, axis=0)
              MC = np.delete(MC, idelete2, axis=0)
              REC = np.delete(REC, idelete2, axis=0)
              REC2 = np.delete(REC2, idelete2, axis=0)
    '''

    ar=np.array(rec_minus)
    ar = K.constant(ar)
    a, m, s=BN(ar)
    rec_minus=np.array(sess.run(a))
    for i in range(0,nvariable):
           Mean_pre.append(sess.run(m)[0,i])
           Std_pre.append(sess.run(s)[0,i])

    # Transformation
    rec_t = mc+(rec[:,0:14]-mc)*(Std_pre[11]/Std[11])*Std[0:14]/Std_pre 
#    rec=np.column_stack((rec,REC))

#    y = Comput_BN([rec_minus, mc])
 #   y = (sess.run(y))[1]
#    print("--------------")
 #   print(y)
  #  print(y-rec_minus)

    # Define train and test (MC, MC_test are only used for plotting)
    test_size=0.3
    random_state=0 
#    MC_train, MC_test, x_train, x_test, y_train, y_test, y_train_t, y_test_t, y_train_minus, y_test_minus = train_test_split(MC, mc, rec, rec_t, rec_minus, test_size=test_size, random_state=random_state)
    MC_train, MC_test, x_train, x_test, y_train, y_test, y_train_t, y_test_t = train_test_split(MC, mc, rec, rec_t, test_size=test_size, random_state=random_state)

    N=mc.shape[0]
    N_test=x_test.shape[0]  

    # Network parameters
    input_shape = (x_train.shape[1],)
    batch_size = 128
    latent_dim = 100

    # Build the Autoencoder Model
    # First build the Encoder Model
    inputs = Input(shape=input_shape, name='Gen_input')

    # Shape info needed to build Decoder Model
    shape = K.int_shape(inputs)

    print("-------------")
    print(shape)

    x1 = inputs

    x1 = Dense(latent_dim, name='Encoder_mae')(x1)
    x1 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_1_mae')(x1)

    # Generate the latent vector
    latent1 = Dense(latent_dim, name='Latent_vector_1_mae')(x1)
    latent1 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_2_mae')(latent1)

    x1 = Dense(latent_dim, name='Latent_vector_2_mae')(latent1)
    x1 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_3_mae')(x1)

    x1 = Dense(latent_dim, name='Latent_vector_3_mae')(x1)
    x1 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_4_mae')(x1)

    x1 = Dense(latent_dim, name='Latent_vector_4_mae')(x1)
    x1 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_5_mae')(x1)

    x1 = Dense(shape[1], name='Decoder_mae')(x1)
    x1 = Activation('linear', name='Linear_mae')(x1)

    x2 = inputs

    x2 = Dense(latent_dim, name='Encoder_quantile')(x2)
    x2 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_1_quantile')(x2)

    # Generate the latent vector
    latent2 = Dense(latent_dim, name='Latent_vector_1_quantile')(x2)
    latent2 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_2_quantile')(latent2)

    x2 = Dense(latent_dim, name='Latent_vector_2_quantile')(latent2)
    x2 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_3_quantile')(x2)

    x2 = Dense(latent_dim, name='Latent_vector_3_quantile')(x2)
    x2 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_4_quantile')(x2)

    x2 = Dense(latent_dim, name='Latent_vector_4_quantile')(x2)
    x2 = Activation(LeakyReLU(alpha=0.05), name='LeakyRelu_5_quantile')(x2)

    x2 = Dense(shape[1], name='Decoder_quantile')(x2)
    x2 = Activation('linear', name='Linear_quantile')(x2)
  
 #   x2 = Dense(shape[1], name='Decoder_mae_2')(x)
  #  x2 = Activation('linear', name='Linear_mae_2')(x2)

    output1, output2, output3 = Lambda(Comput_BN, name='Comput_BN')([x1, x2, inputs])


    autoencoder = Model(inputs, outputs=[output1, output2, output3], name='autoencoder')
    autoencoder.summary()

    if options.train_apply==0:

        os.system("rm weights-*.h5")

        # Train the autoencoder 
   
        weight1=K.variable(0.)
        weight2=K.variable(0.5)
        weight3=K.variable(0.5)
  
        optimizer = Adam(lr=0.001)
        lr_metric = get_lr_metric(optimizer)
 
        autoencoder.compile(loss=['mae', 'mae', quantile_loss], loss_weights=[weight1, weight2, weight3], optimizer=optimizer, metrics=[lr_metric])
   
        lr = LearningRateScheduler(scheduler)
   #     keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=epsilon, decay=1., amsgrad=False)
   
  #      print("autoencoder.optimizer.lr")
   #     print(K.get_value(autoencoder.optimizer.lr))

        # val_loss: total weighted loss; val_Comput_BN_loss: loss of last output before being weighted

    #    lr = ReduceLROnPlateau(monitor='val_loss', patience=10, factor=0.2, mode='min')

        filepath="weights-{epoch:02d}-{val_loss:.7f}.h5"       

        checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=False, save_weights_only=True, mode='auto')
   
        autoencoder.fit(x_train, [np.zeros( (x_train.shape[0], 26) ), y_train_t, y_train_t],
                    validation_data=(x_test, [np.zeros( (x_test.shape[0], 26) ), y_test_t, y_test_t]),
                    epochs=options.epochs,
                    batch_size=batch_size,
                    callbacks=[MyCallback(weight1, weight2, weight3), checkpoint, lr])

        val_loss_min=100.
        final=0
        os.system("ls weights-*.h5 > file.log")
        file=open("file.log")
        for line in file:
                      line_split=line.split("-")
                      val_loss=float((line_split[2])[0:7])
                      n=int(line_split[1])
                      if val_loss<=val_loss_min:
                 #     if val_loss<=val_loss_min and n>epoch_rise:
                                  final=line
                                  val_loss_min=val_loss

        print("-------The best weight is: ---------")
        print(final)

    if options.train_apply!=0:

        weights_file='../physics/weights-499-0.0108859.h5'

        autoencoder.load_weights(weights_file, by_name=True)

        # Predict the Autoencoder output from corrupted test imformation
        x_decoded = autoencoder.predict(x_test)
    
 #       x_decoded=np.column_stack((x_decoded[0], x_decoded[1]))

 #       x_mean=x_decoded[0]
 #       x_sigma=abs(x_decoded[1]-x_decoded[0])
 
 #       x_decoded = np.random.normal(loc=x_mean, scale=x_sigma, size=x_decoded[0].shape)

 #       xx=Comput_BN([x_decoded, x_test])
 #       x1,x2,x3=sess.run(xx)
 #       x_decoded=np.column_stack((x1, x_decoded))

        x_decoded = x_decoded[0]
   
        # Return to the true value
        mc=mc.tolist()
        rec=rec.tolist()
        x_test=x_test.tolist()
        y_test=y_test.tolist()
        x_decoded=x_decoded.tolist()
        branch_rec=["K_eta","K_phi","K_pt","pi_eta","pi_phi","pi_pt","pis_eta","pis_phi","pis_pt","mu_eta","mu_phi","mu_pt","B_eta","B_phi","D0_energy","D0_eta","D0_phi","D0_pt","Dst_energy","Dst_eta","Dst_phi","Dst_pt","B0_pt","M2_miss","q2","Est_mu","K_eta_Rec-Gen","K_phi_Rec-Gen","K_pt_Rec-Gen","pi_eta_Rec-Gen","pi_phi_Rec-Gen","pi_pt_Rec-Gen","pis_eta_Rec-Gen","pis_phi_Rec-Gen","pis_pt_Rec-Gen","mu_eta_Rec-Gen","mu_phi_Rec-Gen","mu_pt_Rec-Gen","B_eta_Rec-Gen","B_phi_Rec-Gen"]
        nvariable=len(branch_rec)
    
        for i in range(0,26):
             for j in range(0,N):
                  MC[j][i]=MC[j][i]*(Std[i]+epsilon)/beta+Mean[i]
                  rec[j][i]=rec[j][i]*(Std[i]+epsilon)/beta+Mean[i]
             for k in range(0,N_test):
                  MC_test[k][i]=MC_test[k][i]*(Std[i]+epsilon)/beta+Mean[i]
                  y_test[k][i]=y_test[k][i]*(Std[i]+epsilon)/beta+Mean[i]

        for i in range(0,26):
             for k in range(0,N_test):
                  x_decoded[k][i]=x_decoded[k][i]*(Std[i]+epsilon)/beta+Mean[i]

        for i in range(0,14):
             for k in range(0,N_test):
                  x_test[k][i]=x_test[k][i]*(Std[i]+epsilon)/beta+Mean[i]

        print("Sigma")
        print(Std)
        print(Std_pre)

        # Draw Model Plots 
        os.system("rm -rf Plots")
        os.system("mkdir Plots")

        plot_model(autoencoder, to_file='./Plots/model.png', show_shapes=True)

        # Draw Comparision Plots
        Epochs=(weights_file.split("-"))[1]
        log_file="draw_loss.log"

        # Draw 14 REC-GEN variables:
        for j in range(0, 14):
            nbin=50
            min=-3.
            max=-min
            min=min*(Std_pre[j]+epsilon)/beta+Mean_pre[j]
            max=max*(Std_pre[j]+epsilon)/beta+Mean_pre[j]
      #      min=min*(0.025*(Std[0:14])[j]/Std_pre[j])*(Std_pre[j]+epsilon)/beta+Mean_pre[j]
       #     max=-min

            lbin=str(round((max-min)/nbin,6))
            c = TCanvas("c","c", 700,700)
            fPads1 = TPad("pad1", "Run2", 0.0, 0.39, 1.00, 1.00)
            fPads2 = TPad("pad2", "", 0.00, 0.00, 1.00, 0.39)
            fPads1.SetTopMargin(  0.15)
            fPads1.SetBottomMargin( 0.007)
            fPads1.SetLeftMargin(   0.10)
            fPads1.SetRightMargin(  0.03)
            fPads2.SetLeftMargin(   0.10 )
            fPads2.SetRightMargin(  0.03)
            fPads2.SetBottomMargin( 0.25)
            fPads1.Draw()
            fPads2.Draw()
            fPads1.cd()
            xtitle=branch_rec[j+26]
            ytitle="Events/"+lbin
            h_rec=TH1D("h_rec",""+";%s;%s"%(xtitle,ytitle),nbin,min,max)
            h_rec.Sumw2()
            h_pre=TH1D("h_pre",""+";%s;%s"%(xtitle,ytitle),nbin,min,max)
            h_pre.Sumw2()
            for i in range(N_test):
                      h_rec.Fill(y_test[i][j]-x_test[i][j])
                      h_pre.Fill(x_decoded[i][j]-x_test[i][j])
            h_rec=UnderOverFlow1D(h_rec)
            h_pre=UnderOverFlow1D(h_pre)

            #CHI2 test
            NBIN=30
            MIN=min
            MAX=max

            h_REC=TH1D("h_rec",""+";%s;%s"%(xtitle,ytitle),NBIN,MIN,MAX)
            h_REC.Sumw2()
            h_PRE=TH1D("h_pre",""+";%s;%s"%(xtitle,ytitle),NBIN,MIN,MAX)
            h_PRE.Sumw2()
            for i in range(N_test):
                      h_REC.Fill(y_test[i][j]-x_test[i][j])
                      h_PRE.Fill(x_decoded[i][j]-x_test[i][j])
            p_value=h_REC.Chi2Test(h_PRE,"UF,OF")
            if p_value>0.99:
                 p_value=1.-float("%.2g" % (1.-p_value))
            else:
                 p_value=round(p_value,3)

            maxR = 2
            maxY = TMath.Max( h_rec.GetMaximum(), h_pre.GetMaximum() )
            h_rec.SetLineColor(2)
            h_rec.SetFillStyle(0)
            h_rec.SetLineWidth(2)
            h_rec.SetLineStyle(1)
            h_pre.SetLineColor(3)
            h_pre.SetFillStyle(0)
            h_pre.SetLineWidth(2)
            h_pre.SetLineStyle(1)
            h_rec.GetYaxis().SetRangeUser( 0 , maxY*1.1 )
            h_rec.GetYaxis().SetTitleSize(0.06)
            h_rec.GetYaxis().SetTitleOffset(0.82)
            h_rec.SetStats(0)
            h_pre.SetStats(0)
            h_rec.Draw("HIST")
            h_pre.Draw("same HIST")
            theLeg = TLegend(0.58, 0.45, 0.99, 0.82, "", "NDC")
            theLeg.SetName("theLegend")
            theLeg.SetBorderSize(0)
            theLeg.SetLineColor(0)
            theLeg.SetFillColor(0)
            theLeg.SetFillStyle(0)
            theLeg.SetLineWidth(0)
            theLeg.SetLineStyle(0)
            theLeg.SetTextFont(42)
            theLeg.SetTextSize(.05)
            theLeg.AddEntry(h_rec,"Rec-Gen","L")
            theLeg.AddEntry(h_pre,"Pre of Rec-Gen","L")
            theLeg.SetY1NDC(0.9-0.05*6-0.005)
            theLeg.SetY1(theLeg.GetY1NDC())
            fPads1.cd()
            theLeg.Draw()
            title = TLatex(0.76,0.93,"AE prediction vs reconstruction, epochs="+Epochs)
            title.SetNDC()
            title.SetTextSize(0.05)
            title.SetTextFont(42)
            title.SetTextAlign(31)
            title.SetLineWidth(2)
            title.Draw()
            # Draw p_value
            title2 = TLatex(0.71,0.88,"#chi^{2} p-val = %.2f"%p_value)
            title2.SetNDC()
            title2.SetTextSize(0.04)
            title2.SetTextFont(42)
            title2.SetTextAlign(31)
            title2.SetLineWidth(2)
            title2.Draw()
            fPads2.cd()
            h_Ratio=TH1D("h_Ratio",""+";%s;%s"%(xtitle,ytitle),nbin,min,max)
            h_Ratio.SetLineColor(3)
            h_Ratio.SetLineWidth(2)
            h_Ratio.SetMarkerColor(3)
            h_Ratio.SetMarkerStyle(8)
            h_Ratio.SetMarkerSize(0.7)
            h_Ratio.SetStats(0)
            h_Ratio.GetYaxis().SetRangeUser( 0 , maxR )
            h_Ratio.GetYaxis().SetNdivisions(504,0)
            h_Ratio.GetYaxis().SetTitle("Ratio")
            h_Ratio.GetYaxis().SetTitleOffset(0.35)
            h_Ratio.GetYaxis().SetTitleSize(0.13)
            h_Ratio.GetYaxis().SetTitleSize(0.13)
            h_Ratio.GetYaxis().SetLabelSize(0.11)
            h_Ratio.GetXaxis().SetLabelSize(0.1)
            h_Ratio.GetXaxis().SetTitleOffset(1.0)
            h_Ratio.GetXaxis().SetTitleSize(0.1)

            axis1=TGaxis( min,1,max,1, 0,0,0, "L")
            axis1.SetLineColor(1)
            axis1.SetLineWidth(1)
            for i in range(1,h_Ratio.GetNbinsX()+1,1):
                            D  = h_pre.GetBinContent(i)
                            eD = h_pre.GetBinError(i)
                            if D==0: eD=0.92
                            B  = h_rec.GetBinContent(i)
                            eB = h_rec.GetBinError(i)
                            if B<0.1 and eB>=B :
                                           eB=0.92
                                           Err= 0.
                            if B!=0.:
                                           Err=TMath.Sqrt((eD*eD)/(B*B)+(D*D*eB*eB)/(B*B*B*B))
                                           h_Ratio.SetBinContent(i, D/B)
                                           h_Ratio.SetBinError(i, Err)
                            if B==0.:
                                           Err=TMath.Sqrt( (eD*eD)/(eB*eB)+(D*D*eB*eB)/(eB*eB*eB*eB) )
                                           h_Ratio.SetBinContent(i, D/0.92)
                                           h_Ratio.SetBinError(i, Err)
                            if D==0 and B==0:
                                           h_Ratio.SetBinContent(i, -1)
                                           h_Ratio.SetBinError(i, 0)
                            if h_Ratio.GetBinContent(i)>maxR:
                                           h_Ratio.SetBinContent(i, maxR)

            h_Ratio.Draw("e0")
            axis1.Draw()
            c.SaveAs("Plots/"+branch_rec[j+26]+"_comparision.png")

            #2D plots Pre vs Rec
            c2 = TCanvas("c2","c2", 700,700)
            fPads3 = TPad("pad3", "Run2", 0.0, 0.0, 1.00, 1.00)
            fPads3.SetBottomMargin( 0.09)
            fPads3.SetLeftMargin(   0.10)
            fPads3.SetRightMargin(  0.15)
            fPads3.Draw()
            fPads3.cd()
            xtitle = branch_rec[j+26]+" (Rec)"
            ytitle = branch_rec[j+26]+" (Pre)"
            h_2D=TH2D("h_2D",""+";%s;%s"%(xtitle,ytitle),nbin,min,max,nbin,min,max)
            h_2D.Sumw2()
            h_2D.SetStats(0)
            for i in range(N_test):
                      h_2D.Fill(y_test[i][j]-x_test[i][j],x_decoded[i][j]-x_test[i][j])
            h_2D.GetYaxis().SetTitleSize(0.04)
            h_2D.GetYaxis().SetTitleOffset(1.20)
            h_2D = UnderOverFlow2D(h_2D)
            h_2D.Draw("colz")
            title = TLatex(0.83,0.93,"AE prediction vs reconstruction, epochs="+Epochs)
            title.SetNDC()
            title.SetTextSize(0.04)
            title.SetTextFont(42)
            title.SetTextAlign(31)
            title.SetLineWidth(2)
            title.Draw()
            c2.SaveAs("Plots/2D_"+branch_rec[j+26]+"_Pre_vs_Rec_comparision.png")

        # Draw 26 Regression variables:
        for j in range(0, 26):
            nbin=50
            min=-3.
            max=-min
            min=min*(Std[j]+epsilon)/beta+Mean[j]
            max=max*(Std[j]+epsilon)/beta+Mean[j]
            if j==23:
                 min=-3.
                 max=6.
            if j==24:
                 min=-2.
                 max=10.7
            if j==25:
                 min=0.
                 max=2.5
            lbin=str(round((max-min)/nbin,3))              
            c = TCanvas("c","c", 700,700)
            fPads1 = TPad("pad1", "Run2", 0.0, 0.39, 1.00, 1.00)
            fPads2 = TPad("pad2", "", 0.00, 0.00, 1.00, 0.39)
            fPads1.SetTopMargin(  0.15)
            fPads1.SetBottomMargin( 0.007)
            fPads1.SetLeftMargin(   0.10)
            fPads1.SetRightMargin(  0.03)
            fPads2.SetLeftMargin(   0.10 )
            fPads2.SetRightMargin(  0.03) 
            fPads2.SetBottomMargin( 0.25)
            fPads1.Draw()
            fPads2.Draw()
            fPads1.cd()
            xtitle=branch_rec[j]
            ytitle="Events/"+lbin
            h_gen=TH1D("h_gen",""+";%s;%s"%(xtitle,ytitle),nbin,min,max)
            h_gen.Sumw2()
            h_rec=TH1D("h_rec",""+";%s;%s"%(xtitle,ytitle),nbin,min,max)
            h_rec.Sumw2()
            h_pre=TH1D("h_pre",""+";%s;%s"%(xtitle,ytitle),nbin,min,max)  
            h_pre.Sumw2()
            for i in range(N_test):
                      h_gen.Fill(MC_test[i][j])
                      h_rec.Fill(y_test[i][j])
                      h_pre.Fill(x_decoded[i][j])
            h_gen=UnderOverFlow1D(h_gen)
            h_rec=UnderOverFlow1D(h_rec)
            h_pre=UnderOverFlow1D(h_pre)

            #CHI2 test
            NBIN=30
            MIN=min
            MAX=max
            # Eta
            if j==0 or j==3 or j==6 or j==9 or j==12 or j==15 or j==19:
                              MIN=-2.3
                              MAX=2.3
            # Phi
            if j==1 or j==4 or j==7 or j==10 or j==13 or j==16 or j==20:
                              MIN=-3.1
                              MAX=3.1
            # Pt
            if j==2 or j==5:
                              MIN=2.
                              MAX=25.
            if j==8:
                              MIN=0.5
                              MAX=3.5
            if j==11:
                              MIN=10.
                              MAX=35.
            if j==17 or j==21:
                              MIN=5.
                              MAX=40.
            if j==22:
                              MIN=15.
                              MAX=90.
            # E
            if j==14 or j==18:
                              MIN=10.
                              MAX=70.
            # Three target variables
            if j==23:
                              MIN=-2.
                              MAX=6.
            if j==24:
                              MIN=-1.
                              MAX=10.
            if j==25:
                              MIN=0.5
                              MAX=2.1

            h_REC=TH1D("h_rec",""+";%s;%s"%(xtitle,ytitle),NBIN,MIN,MAX)
            h_REC.Sumw2()
            h_PRE=TH1D("h_pre",""+";%s;%s"%(xtitle,ytitle),NBIN,MIN,MAX)
            h_PRE.Sumw2()
            for i in range(N_test):
                      h_REC.Fill(y_test[i][j])
                      h_PRE.Fill(x_decoded[i][j])
         #   p_value=h_REC.Chi2Test(h_PRE,"UF,OF")
            p_value=h_REC.Chi2Test(h_PRE,"UF,OF")
            if p_value>0.99:
                 p_value=1.-float("%.2g" % (1.-p_value))
            else:
                 p_value=round(p_value,3)
         #   p_value=str(p_value)

            maxR = 2
            maxY = TMath.Max( h_rec.GetMaximum(), h_pre.GetMaximum() ) 
            maxY = TMath.Max( maxY, h_gen.GetMaximum() )
            h_gen.SetLineColor(4)
            h_gen.SetFillStyle(0)
            h_gen.SetLineWidth(2)
            h_gen.SetLineStyle(1)
            h_rec.SetLineColor(2)
            h_rec.SetFillStyle(0)
            h_rec.SetLineWidth(2)
            h_rec.SetLineStyle(1)
            h_pre.SetLineColor(3) 
            h_pre.SetFillStyle(0)
            h_pre.SetLineWidth(2)
            h_pre.SetLineStyle(1) 
            h_gen.SetStats(0)  
            h_rec.SetStats(0)
            h_pre.SetStats(0)
            h_gen.GetYaxis().SetRangeUser( 0 , maxY*1.1 )
            h_gen.GetYaxis().SetTitleSize(0.06)
            h_gen.GetYaxis().SetTitleOffset(0.82)
            h_gen.Draw("HIST")
            h_rec.Draw("same HIST")
            h_pre.Draw("same HIST")
            theLeg = TLegend(0.58, 0.45, 0.99, 0.82, "", "NDC")
            theLeg.SetName("theLegend")
            theLeg.SetBorderSize(0)
            theLeg.SetLineColor(0)
            theLeg.SetFillColor(0)
            theLeg.SetFillStyle(0)
            theLeg.SetLineWidth(0)
            theLeg.SetLineStyle(0)
            theLeg.SetTextFont(42)
            theLeg.SetTextSize(.05)
            theLeg.AddEntry(h_gen,"Generation","L")
            theLeg.AddEntry(h_rec,"Reconstruction","L")
            theLeg.AddEntry(h_pre,"Prediction","L")
            theLeg.SetY1NDC(0.9-0.05*6-0.005)
            theLeg.SetY1(theLeg.GetY1NDC())
            fPads1.cd()
            theLeg.Draw()
            title = TLatex(0.76,0.93,"AE prediction vs reconstruction, epochs="+Epochs)
            title.SetNDC()
            title.SetTextSize(0.05)  
            title.SetTextFont(42)
            title.SetTextAlign(31)  
            title.SetLineWidth(2)
            title.Draw()
            # Draw p_value
            title2 = TLatex(0.71,0.88,"#chi^{2} p-val = %.2f"%p_value)
            title2.SetNDC()
            title2.SetTextSize(0.04)
            title2.SetTextFont(42)
            title2.SetTextAlign(31)
            title2.SetLineWidth(2)
            title2.Draw()
            fPads2.cd()
            h_Ratio=TH1D("h_Ratio",""+";%s;%s"%(xtitle,ytitle),nbin,min,max)
            h_Ratio.SetLineColor(3)
            h_Ratio.SetLineWidth(2)
            h_Ratio.SetMarkerColor(3)
            h_Ratio.SetMarkerStyle(8)
            h_Ratio.SetMarkerSize(0.7)
            h_Ratio.GetYaxis().SetRangeUser( 0 , maxR )
            h_Ratio.GetYaxis().SetNdivisions(504,0)
            h_Ratio.GetYaxis().SetTitle("Ratio")
            h_Ratio.GetYaxis().SetTitleOffset(0.35)
            h_Ratio.GetYaxis().SetTitleSize(0.13)
            h_Ratio.GetYaxis().SetTitleSize(0.13)
            h_Ratio.GetYaxis().SetLabelSize(0.11)
            h_Ratio.GetXaxis().SetLabelSize(0.1)
            h_Ratio.GetXaxis().SetTitleOffset(1.0)
            h_Ratio.GetXaxis().SetTitleSize(0.1)
            h_Ratio.SetStats(0)
            axis1=TGaxis( min,1,max,1, 0,0,0, "L")
            axis1.SetLineColor(3)
            axis1.SetLineWidth(1)
            for i in range(1,h_Ratio.GetNbinsX()+1,1):
                            D  = h_pre.GetBinContent(i)
                            eD = h_pre.GetBinError(i)
                            if D==0: eD=0.92
                            B  = h_rec.GetBinContent(i)
                            eB = h_rec.GetBinError(i)
                            if B<0.1 and eB>=B :
                                           eB=0.92
                                           Err= 0.
                            if B!=0.: 
                                           Err=TMath.Sqrt((eD*eD)/(B*B)+(D*D*eB*eB)/(B*B*B*B))
                                           h_Ratio.SetBinContent(i, D/B)
                                           h_Ratio.SetBinError(i, Err)
                            if B==0.:
                                           Err=TMath.Sqrt( (eD*eD)/(eB*eB)+(D*D*eB*eB)/(eB*eB*eB*eB) )
                                           h_Ratio.SetBinContent(i, D/0.92)
                                           h_Ratio.SetBinError(i, Err)
                            if D==0 and B==0:  
                                           h_Ratio.SetBinContent(i, -1)
                                           h_Ratio.SetBinError(i, 0)
                            if h_Ratio.GetBinContent(i)>maxR:
                                           h_Ratio.SetBinContent(i, maxR)
    
            axis1=TGaxis( min,1,max,1, 0,0,0, "L")
            axis1.SetLineColor(1)
            axis1.SetLineWidth(1)
    
            h_Ratio.Draw("e0")
            axis1.Draw()
    
            c.SaveAs("Plots/"+branch_rec[j]+"_comparision.png")
            
            #2D plots Pre vs Rec
            c2 = TCanvas("c2","c2", 700,700)
            fPads3 = TPad("pad3", "Run2", 0.0, 0.0, 1.00, 1.00)
            fPads3.SetBottomMargin( 0.09)
            fPads3.SetLeftMargin(   0.10)
            fPads3.SetRightMargin(  0.15)
            fPads3.Draw()
            fPads3.cd()
            xtitle = branch_rec[j]+" (Rec)"
            ytitle = branch_rec[j]+" (Pre)"
            h_2D=TH2D("h_2D",""+";%s;%s"%(xtitle,ytitle),nbin,min,max,nbin,min,max)
            h_2D.Sumw2()
            h_2D.SetStats(0)
            for i in range(N_test):
                      h_2D.Fill(y_test[i][j],x_decoded[i][j])
            h_2D.GetYaxis().SetTitleSize(0.04)
            h_2D.GetYaxis().SetTitleOffset(1.20)
            h_2D = UnderOverFlow2D(h_2D)
            h_2D.Draw("colz")
            title = TLatex(0.83,0.93,"AE prediction vs reconstruction, epochs="+Epochs)
            title.SetNDC()
            title.SetTextSize(0.04)
            title.SetTextFont(42)
            title.SetTextAlign(31)
            title.SetLineWidth(2)
            title.Draw()
            c2.SaveAs("Plots/2D_"+branch_rec[j]+"_Pre_vs_Rec_comparision.png")
    
            #2D plots Gen vs Rec
            c3 = TCanvas("c3","c3", 700,700)
            fPads4 = TPad("pad4", "Run2", 0.0, 0.0, 1.00, 1.00)
            fPads4.SetBottomMargin( 0.09)
            fPads4.SetLeftMargin(   0.10)
            fPads4.SetRightMargin(  0.15)
            fPads4.Draw()
            fPads4.cd()
            xtitle = branch_rec[j]+" (Rec)"
            ytitle = branch_rec[j]+" (Gen)"
            h_2D_2=TH2D("h_2D_2",""+";%s;%s"%(xtitle,ytitle),nbin,min,max,nbin,min,max)
            h_2D_2.Sumw2()
            h_2D_2.SetStats(0)
            for i in range(N):
                      h_2D_2.Fill(rec[i][j],MC[i][j])
            h_2D_2.GetYaxis().SetTitleSize(0.04)
            h_2D_2.GetYaxis().SetTitleOffset(1.20)
            h_2D_2 = UnderOverFlow2D(h_2D_2)
            h_2D_2.Draw("colz")
            title = TLatex(0.80,0.93,"Generation vs reconstruction, epochs="+Epochs)
            title.SetNDC()
            title.SetTextSize(0.04)
            title.SetTextFont(42)
            title.SetTextAlign(31)
            title.SetLineWidth(2)
            title.Draw()
            c3.SaveAs("Plots/2D_"+branch_rec[j]+"_Gen_vs_Rec_comparision.png")
           
            #2D plots Gen vs Pre
            c4 = TCanvas("c4","c4", 700,700)
            fPads5 = TPad("pad5", "Run2", 0.0, 0.0, 1.00, 1.00)
            fPads5.SetBottomMargin( 0.09)
            fPads5.SetLeftMargin(   0.10)
            fPads5.SetRightMargin(  0.15)
            fPads5.Draw()
            fPads5.cd()
            xtitle = branch_rec[j]+" (Pre)"
            ytitle = branch_rec[j]+" (Gen)"
            h_2D_3=TH2D("h_2D_3",""+";%s;%s"%(xtitle,ytitle),nbin,min,max,nbin,min,max)
            h_2D_3.Sumw2()
            h_2D_3.SetStats(0)
            for i in range(N_test):
                      h_2D_3.Fill(x_decoded[i][j],MC_test[i][j])
            h_2D_3.GetYaxis().SetTitleSize(0.04)
            h_2D_3.GetYaxis().SetTitleOffset(1.20)
            h_2D_3 = UnderOverFlow2D(h_2D_3)
            h_2D_3.Draw("colz")
            title = TLatex(0.80,0.93,"Generation vs AE prediction, epochs="+Epochs)
            title.SetNDC()
            title.SetTextSize(0.04)
            title.SetTextFont(42)
            title.SetTextAlign(31)
            title.SetLineWidth(2)
            title.Draw()
            c4.SaveAs("Plots/2D_"+branch_rec[j]+"_Gen_vs_Pre_comparision.png")
            
        #Draw loss vs epochs
        file=open(log_file)
        os.system("grep val_loss: "+log_file+" > out.log")
        file=open("out.log")
        lines=[]
        loss=[]
        epochs=[]
        val_loss=[]
        for line in file:
                 lines.append(line)
        for line in lines:
                 line=line.split(":")
                 loss.append(float((line[1].split(" "))[1]))
                 if "val_loss" in line[5]:
                       val_loss.append(float((line[6].split(" "))[1]))
                 else:
                       print("-----read val_loss error------")
        for i in range(1,len(loss)+1):
                 epochs.append(i)
    
        c5 = TCanvas("c5","c5", 700,700)
        fPads6 = TPad("pad6", "Run2", 0.0, 0.0, 1.00, 1.00)
        fPads6.SetBottomMargin( 0.08)
        fPads6.SetLeftMargin(   0.15)
        fPads6.SetRightMargin(  0.05)
        fPads6.SetTopMargin(  0.07)
        fPads6.Draw()
        fPads6.cd()
        g1 = TGraph()
     #   for i in range(1500,len(loss)+1):
    #    for i in range(1,len(loss)+1):
        for i in range(1,500):
                g1.SetPoint(i,epochs[i-1],loss[i-1])
        g1.SetLineColor(kBlue)
        g1.SetLineWidth(2)
        g1.SetMarkerColor(kBlue)
        g1.SetMarkerStyle(2)
    
        g2 = TGraph()
      #  for i in range(1500,len(loss)+1):
     #   for i in range(1,len(loss)+1):
        for i in range(1,500):
                g2.SetPoint(i,epochs[i-1],val_loss[i-1])
        g2.SetLineColor(kRed)
        g2.SetLineWidth(2)
        g2.SetMarkerColor(kRed)
        g2.SetMarkerStyle(2)
    
        mg = TMultiGraph()
        mg.Add(g1)
        mg.Add(g2)
        mg.Draw("apl")
        mg.SetTitle("Loss vs epochs")
        mg.GetYaxis().SetTitle("Loss")
        mg.GetXaxis().SetTitle("Epochs")
        mg.GetXaxis().SetRangeUser(1.5,500)
      #  mg.GetXaxis().SetRangeUser(1.5,len(loss))
      #  mg.GetXaxis().SetRangeUser(1500.,len(loss))
    
        theLeg = TLegend(0.48, 0.52, 0.89, 0.89, "", "NDC")
        theLeg.SetName("theLegend")
        theLeg.SetBorderSize(0)
        theLeg.SetLineColor(0)
        theLeg.SetFillColor(0)
        theLeg.SetFillStyle(0)
        theLeg.SetLineWidth(0)
        theLeg.SetLineStyle(0)
        theLeg.SetTextFont(42)
        theLeg.SetTextSize(.05)
        theLeg.AddEntry(g1,"Training loss","L")
        theLeg.AddEntry(g2,"Validation loss","L")
        theLeg.SetY1NDC(0.9-0.05*6-0.005)
        theLeg.SetY1(theLeg.GetY1NDC())
        theLeg.Draw()
    
        c5.SaveAs("Plots/Loss.png")

def UnderOverFlow1D(h):
    Bins=h.GetNbinsX()
    h.SetBinContent( 1,  h.GetBinContent(1)+h.GetBinContent(0) )
    h.SetBinError(   1,  math.sqrt( h.GetBinError(1)*h.GetBinError(1) + h.GetBinError(0)*h.GetBinError(0)) )
    h.SetBinContent( Bins,  h.GetBinContent(Bins)+h.GetBinContent(Bins+1) )
    h.SetBinError(   Bins,  math.sqrt( h.GetBinError(Bins)*h.GetBinError(Bins) + h.GetBinError(Bins+1)*h.GetBinError(Bins+1)) )
    return h

def UnderOverFlow2D(h):
    BinsX=h.GetNbinsX()
    BinsY=h.GetNbinsY()
    for i in range(0,BinsX):
          h.SetBinContent( i+1, 1,  h.GetBinContent(i+1, 1)+h.GetBinContent(i+1, 0) )
          h.SetBinError( i+1, 1,  math.sqrt(h.GetBinError(i+1, 1)*h.GetBinError(i+1, 1)+h.GetBinError(i+1, 0)*h.GetBinError(i+1, 0)) )
          h.SetBinContent( i+1, BinsY,  h.GetBinContent(i+1, BinsY)+h.GetBinContent(i+1, BinsY+1) )
          h.SetBinError( i+1, BinsY,  math.sqrt(h.GetBinError(i+1, BinsY)*h.GetBinError(i+1, BinsY)+h.GetBinError(i+1, BinsY+1)*h.GetBinError(i+1, BinsY+1)) )
          h.SetBinContent( 1, i+1,  h.GetBinContent(1, i+1)+h.GetBinContent(0, i+1) )
          h.SetBinError( 1, i+1,  math.sqrt(h.GetBinError(1, i+1)*h.GetBinError(1, i+1)+h.GetBinError(0, i+1)*h.GetBinError(0, i+1)) )
          h.SetBinContent( BinsX, i+1,  h.GetBinContent(BinsX, i+1)+h.GetBinContent(BinsX+1, i+1) )
          h.SetBinError( BinsX, i+1,  math.sqrt(h.GetBinError(BinsX, i+1)*h.GetBinError(BinsX, i+1)+h.GetBinError(BinsX+1, i+1)*h.GetBinError(BinsX+1, i+1)) )
    h.SetBinContent( 1, 1,  h.GetBinContent(1, 1)+h.GetBinContent(0, 0) )
    h.SetBinError( 1, 1,  math.sqrt(h.GetBinError(1, 1)*h.GetBinError(1, 1)+h.GetBinError(0, 0)*h.GetBinError(0, 0)) )
    h.SetBinContent( BinsX, 1,  h.GetBinContent(BinsX, 1)+h.GetBinContent(BinsX+1, 0) )
    h.SetBinError( BinsX, 1,  math.sqrt(h.GetBinError(BinsX, 1)*h.GetBinError(BinsX, 1)+h.GetBinError(BinsX+1, 0)*h.GetBinError(BinsX+1, 0)) )
    h.SetBinContent( 1, BinsY,  h.GetBinContent(1, BinsY)+h.GetBinContent(0, BinsY+1) )
    h.SetBinError( 1, BinsY,  math.sqrt(h.GetBinError(1, BinsY)*h.GetBinError(1, BinsY)+h.GetBinError(0, BinsY+1)*h.GetBinError(0, BinsY+1)) )
    h.SetBinContent( BinsX, BinsY,  h.GetBinContent(BinsX, BinsY)+h.GetBinContent(BinsX+1, BinsY+1) )
    h.SetBinError( BinsX, BinsY,  math.sqrt(h.GetBinError(BinsX, BinsY)*h.GetBinError(BinsX, BinsY)+h.GetBinError(BinsX+1, BinsY+1)*h.GetBinError(BinsX+1, BinsY+1)) )
    return h

def array2D_float(array):
    for i in range(0,array.shape[0]):
           for j in range(0,array.shape[1]):
                    array[i][j]=array[i][j][0]
    return array

def quantile_loss(target, pred):
    alpha = 0.841 #0.66
    err = target - pred
    return K.tf.where(err>=0, alpha*err, (alpha-1)*err)

def mean_std_cut(arr, r):
    arr=arr.tolist()
    arr = sorted(arr)
    arr = arr[int(r*len(arr)):int((1-r)*len(arr))]
    arr = K.constant(arr)
    mean = K.mean(arr)
    std = K.std(arr)
    return [mean,std]

def BN(arr):
    mean = K.mean(arr, axis=0, keepdims=True)
    std = K.std(arr, axis=0, keepdims=True)
    arr_normed=(arr-mean)*beta/(std+epsilon)
    return [arr_normed, mean, std]

def pxpypz(arr):
        arr2=arr.tolist()
        P_K=TLorentzVector()
        P_Pi=TLorentzVector()
        P_Pis=TLorentzVector()
        P_Mu=TLorentzVector()
        for i in range(0, arr.shape[0]):
              P_K.SetPtEtaPhiM(arr[i][2],arr[i][0],arr[i][1],M_K)
              P_Pi.SetPtEtaPhiM(arr[i][5],arr[i][3],arr[i][4],M_Pi)
              P_Pis.SetPtEtaPhiM(arr[i][8],arr[i][6],arr[i][7],M_Pi)
              P_Mu.SetPtEtaPhiM(arr[i][11],arr[i][9],arr[i][10],M_Mu)
              arr2[i][0]=P_K.Px()
              arr2[i][1]=P_K.Py()
              arr2[i][2]=P_K.Pz()
              arr2[i][3]=P_Pi.Px()
              arr2[i][4]=P_Pi.Py()
              arr2[i][5]=P_Pi.Pz()
              arr2[i][6]=P_Pis.Px()
              arr2[i][7]=P_Pis.Py()
              arr2[i][8]=P_Pis.Pz()
              arr2[i][9]=P_Mu.Px()
              arr2[i][10]=P_Mu.Py()
              arr2[i][11]=P_Mu.Pz()
        return np.array(arr2)

def remove_num(arr, r):
    arr=np.array(arr)
    n=arr.shape[0]
    a=np.arange(n)
    arr=np.column_stack((arr, a))
    arr = sorted(arr,key=lambda s: s[0])
    m=int(n*r)
    num=[]
    for i in range(0,m):
         num.append(arr[i][1])
    for i in range(n-m,n):
         num.append(arr[i][1])
    return num

def get_lr_metric(optimizer):

        def lr(y_true, y_pred):

                return optimizer.lr

        return lr

if __name__ == '__main__':              
    train_and_apply()
